<?php
class Model_item_country extends Model_basic {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();

        $this->table_name ="items_countries";
    }

	//----------------------------
	function delete ( $where)
	//----------------------------
	{
		$this->db->delete( $this->table_name, $where); 
	}

}