<?php
class Model_basic extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();

        $this->table_name = false;
    }
	
	//----------------------------
    function get( $query_params /*$param, $order=false, $limit=false*/ )
    //----------------------------
	{
        //table

		//select
		if ( isset($query_params->select)){
			$this->db->select( $query_params->select );	
		}

		$this->db->from  ( $this->table_name);

		//joins
		if ( isset( $query_params->joins)){
			foreach ( $query_params->joins as $key => $value ){
				$this->db->join( $key, $value);
			}
		}	
		
		//parameters
		if ( isset($query_params->where))	
		{
			foreach ( $query_params->where as $key => $value )
			{	$this->db->where ($key, $value );		}	
		}
	
		//order
		if ( isset($query_params->order))
		{
			foreach ( $query_params->order as $key => $value )
			{	$this->db->order_by($key, $value); 		}
		}
		//limit
		if ( isset($query_params->limit))
		{	$this->db->limit($query_params->limit); 		}		
		

		//get
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 )
		{
			if ( isset ( $query_params->limit ) AND $query_params->limit == 1 )
				return $query->row();
			else
				return $query->result();
		}
		else
			return false;
    }

	//----------------------------
    function create ( $data )
	//----------------------------
    {
		$this->db->insert( $this->table_name, $data);
    }


	//----------------------------
	function delete ( $column, $id )
	//----------------------------
	{
		$this->db->delete( $this->table_name, array( $column => $id )); 
	}

	
	//----------------------------
    function update ( $query_params, $data )
	//----------------------------
    {
		foreach ( $query_params->where as $key => $value )
		{	$this->db->where ($key, $value );		}		
        $this->db->update($this->table_name, $data);
    }	
    
}