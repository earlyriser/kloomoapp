<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);		
	}

	//INDEX
	public function index( $city_id = false )
	{

		//loads
		$this->load->helper('common');
		$this->load->library('parser');

		//vars
		$user_id = $this->session->userdata('user_id');
		$user_logged = $this->session->userdata('logged_in');
		$city_id = $this->_get_city ( $city_id, $user_logged);
		$data['user_listings'] = "{}";
		$data['user_offers'] = "{}";	
		$data['userdata']= $this->session->all_userdata();

		//get user items if logged and in his city
		if ( $user_logged AND $this->session->userdata('city_id') == $city_id ){
			$data['user_listings'] = get_user_listings( $user_id );
			$data['user_offers'] = get_user_offers( $user_id );	
		}

		//user items
		$data['user_has_items'] = ($data['user_listings'] && $data['user_listings'] !="{}") or ( $data['user_offers'] && $data['user_offers'] !="{}");

		//queries
		$data['city_object'] = get_city_object( $city_id ) ;

		//view
		$data['view'] = 'home';
		$data['user_logged'] = $user_logged;
		$this->load->view('template', array( 'data' => $data) );
	}


	//-----------------------------------------------
	public function listing ($item_id=false) {
	//-----------------------------------------------
		$user_logged = $this->session->userdata('logged_in');

		//view
		$data['view'] = 'form_listing';
		$data['user_logged'] = $user_logged;
		$this->load->view('template', array( 'data' => $data) );		
	}


	//-----------------------------------------------
	public function offer ($item_id=false) {
	//-----------------------------------------------
		$user_logged = $this->session->userdata('logged_in');

		//view
		$data['view'] = 'form_offer';
		$data['user_logged'] = $user_logged;
		$this->load->view('template', array( 'data' => $data) );		
	}

	//-----------------------------------------------
	private function _get_city ( $city_id, $user_logged )
	//-----------------------------------------------
	{
		if ( !$city_id) 
		{
			if ( $user_logged )
			{
				if ( $this->session->userdata('city_id') ){
					return $this->session->userdata('city_id'); 
				}else{
					create_alert( "danger", 'alert no city' );
					redirect ('account/profile_edit');
					return false;
				}	
			}else
			{
				if ( $this->session->userdata('city_id') ){
					return $this->session->userdata('city_id'); 
				}else{
					return 'new_york';
				}				
		
			}
		}else{
			return $city_id;
		}
	}	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */