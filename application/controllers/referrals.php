<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class referrals extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);		
	}
	
	//INDEX
	public function id( $user_id = false )
	{
		redirect ('forms/signup/'.$user_id);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */