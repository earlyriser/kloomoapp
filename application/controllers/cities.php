<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cities extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);		
	}
	
	//INDEX
	public function index( $city_id = false )
	{
		//model
		$this->load->model('Model_city');	

		//query
		$query_params = new StdClass;
		$data['cities'] = $this->Model_city->get( $query_params );

		//view
		$data['view'] 	= 'cities';
		$this->load->view ('template', array('data' => $data)) ;	
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */