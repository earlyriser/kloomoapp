<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offers extends CI_Controller {


	public function __construct()
	{
	    parent::__construct();
	    //$this->output->enable_profiler(TRUE);

		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);

		//model
		$this->load->model('Model_offer');
		$this->load->helper('common');    
	}

	//-----------------------------------------------
	public function create( $item_id )
	//-----------------------------------------------
	{
		//not logged
		if ( !$this->session->userdata('logged_in') )
		{	redirect('forms/login'); }

		//load
		$this->load->helper('string');
		$this->load->Model('Model_user');
		$this->load->Model('Model_item');
		$this->load->library('form_validation');		

		//params
		$qp_user = new StdClass;
		$qp_user->where = array("user_id" => $this->session->userdata('user_id'));
		$qp_user->limit = 1;

		$qp_item = new StdClass;
		$qp_item->where = array("item_id" => $item_id);
		$qp_item->limit = 1;

		//queries
		$item = $this->Model_item->get( $qp_item );

		//redirect to home if no item
		if ( !$item ){
			redirect();
		}

		$data['def_prefix'] = common_get_def_prefix( $item->category_id);

		//validation rules
		$this->form_validation->set_rules('amount', 'Amount', 'required|numeric');
		$this->form_validation->set_rules('condition', 'Condition', 'required|callback_offer_condition_check');

		//validation
		if ($this->form_validation->run() === true){			
			$user = $this->Model_user->get( $qp_user );
			$offer = $this->_prepare_offer( $item_id );			
			
			if ( $this->is_offerable( $item_id, $user, $offer['offer_start'] ) )
			{	
				$this->Model_offer->create( $offer );
				//update_credits ( -1, $user );
				//alert
				create_alert ( "success", "alert creation offer");
				redirect ('home');			
			}		
		}

		//view
		$data['view'] = 'form_offer';
		$data['item'] = $item;
		$this->load->view('template', array( 'data' => $data) );			
	}


	//-------------------------------------
	public function offer_condition_check ( $condition )
	//-------------------------------------
	{
		if ( $condition== 'i' || $condition=='ii' || $condition='iii')
			return true;
		return false;
	}

	//-----------------------------------------------
	public function delete( $item_id ){
	//-----------------------------------------------
		//not logged
		if ( !$this->session->userdata('logged_in') )
		{	redirect('forms/login'); }

		//loads
		$this->load->model('Model_item');

		//vars
		$user_id = $this->session->userdata('user_id');	
		$today = date("Y-m-d");

		//params
		$qp_offer = new StdClass;
		$qp_offer->where = array("offers.item_id" => $item_id,"user_id" => $user_id, "offer_end >=" => $today);
		$qp_offer->limit = 1;	
		$qp_offer->joins = array( "items" => 'items.item_id = offers.item_id');
		
		//queries
		$data['offer'] = $this->Model_offer->get ( $qp_offer );

		//redirect home if no offer in DB
		if ( !$data['offer']){
			redirect ('');
		}

		//if form submitted, unoffer
		if ( $_POST ){
			$qp_offer->joins = false;
			$data = array ("offer_end" => date('Y-m-d', strtotime("-1 day")) );
			$this->Model_offer->update ( $qp_offer, $data );
			//alert
			create_alert ( "success", "alert deletion offer");
			//redirect
			redirect ('home');
		}	

		//view
		$data['view'] = 'form_unoffer';
		$this->load->view('template', array( 'data' => $data) );			
	}

	//-----------------------------------------------
	public function id ( $offer_id ){
	//-----------------------------------------------
		//not logged
		if ( !$this->session->userdata('logged_in') )
		{	redirect('forms/login'); }

		//load
		$this->load->model('Model_listing');	

		//vars
		$today = date("Y-m-d");
		$show = false;
		$user_id = $this->session->userdata('user_id');

		//params offer
		$query_params_offer = new StdClass;
		$query_params_offer->where = array(
			"offer_id" => $offer_id,
			"offer_end >="	=> $today
		);
		$query_params_offer->limit = 1;
		$query_params_offer->joins = array( 
			"items" => 'items.item_id = offers.item_id',
			"users" => 'users.user_id = offers.user_id'
		);	

		//query
		$offer = $this->Model_offer->get ( $query_params_offer );

		if ( $offer )
		{
			//params listing
			$query_params_listing = new StdClass;
			$query_params_listing->where = array(
					"listings.item_id" => $offer->item_id,
					"listing_end >="	=> $today,
					"user_id" => $user_id
			);
			$query_params_listing->limit = 1;

			//query
			$show = $this->Model_listing->get ( $query_params_listing );
		}

		//view
		$data['offer'] = $offer;
		$data['show'] = $show;
		$data['view'] = "offer";
		$this->load->view ('template', array('data' => $data)) ;		
	}

	//-----------------------------------------------
	public function item ( $item_id ){
	//-----------------------------------------------
		//not logged
		if ( !$this->session->userdata('logged_in') )
		{	redirect('forms/login'); }

		$today = date("Y-m-d");
		$user_id = $this->session->userdata('user_id');

		//model
		$this->load->model('Model_listing');

		//params
		$query_params_listing = new StdClass;
		$query_params_listing->where = array(
			"listings.item_id" => $item_id,
			"listing_end >="	=> $today,
			"user_id" => $user_id
		);
		$query_params_listing->limit = 1;
		$query_params_listing->joins = array( 
			"items" => 'items.item_id = listings.item_id'
		);			

		$query_params_offers = new StdClass;
		$query_params_offers->where = array(
			"item_id" => $item_id,
			"city_id" => $this->session->userdata('city_id'),
			"offer_end >=" => $today
		);
		$query_params_offers->joins = array( 
			"users" => 'users.user_id = offers.user_id'
		);		

		$listing = $this->Model_listing->get ( $query_params_listing );
		$offers = false;

		if ( $listing ){
			$offers = $this->Model_offer->get ( $query_params_offers );
		}

		//view
		$data['listing'] = $listing;
		$data['offers'] = $offers;
		$data['view'] = "offers";
		$this->load->view ('template', array('data' => $data)) ;		

	}

	//-----------------------------------------------
	private function _prepare_offer ( $item_id ){
	//-----------------------------------------------
		return	
		array(
			'offer_id' 	=> random_string('unique'),
			'user_id'		=> $this->session->userdata('user_id'),
			'item_id'		=> $item_id,
			'offer_amount'	=> $this->input->post('amount'),
			'offer_condition' => $this->input->post('condition'),
			'offer_start'	=>  date("Y-m-d"),
			'offer_end'	=> date('Y-m-d', strtotime("+1 month"))
			);		
	}

	//-----------------------------------------------
	private function is_offerable( $item_id, $user, $current_time ){
	//-----------------------------------------------
		if ( $this->is_unique_item( $item_id, $user->user_id, $current_time ) )
		{
			return true;
		}

		return false;
	}

	//-----------------------------------------------
	private function is_unique_item( $item_id, $user_id, $current_time ){
	//-----------------------------------------------

		$query_params = new StdClass;
		$query_params->where = array( 
				"offer_end >=" => $current_time,
				"item_id" => $item_id,
				"user_id" => $user_id
			);
		$query_params->limit = 1;
		$query_params->order = array( "item_id" => 'ASC');

		if ( $this->Model_offer->get( $query_params ) )
			return false;

		return true;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */


