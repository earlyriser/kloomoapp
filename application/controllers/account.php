<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);

//$this->output->enable_profiler(TRUE);		
	}

	//INDEX
	public function index(  )
	{

		if ( !$this->session->userdata('logged_in') )
			redirect ('home');

		//loads
		$this->load->helper('common');
		$this->load->model('Model_user');

		//vars
		$user_id = $this->session->userdata('user_id');

		//params
		$qp = new StdClass;
		$qp->where = array(
				"user_id" => $user_id
		);
		$qp->limit = 1;

		//queries
		$data['user'] = $this->Model_user->get( $qp );
		$data['view'] = 'account_profile';
		$data['selected'] ="profile";
		$this->load->view('template', array( 'data' => $data) );
	}


	//PROFILE EDIT
	public function profile_edit ()
	{

		if ( !$this->session->userdata('logged_in') )
			redirect ('home');

		//loads
		$this->load->helper('common');
		$this->load->model('Model_user');
		$this->load->model('Model_city');
		$this->load->library('form_validation');
		$this->load->library('__');	

		//vars
		$user_id = $this->session->userdata('user_id');

		//params user
		$qp = new StdClass;
		$qp->where = array(
				"user_id" => $user_id
		);
		$qp->limit = 1;

		//params cities
		$qp_cities = new StdClass;
		$data['cities'] = $this->Model_city->get( $qp_cities );
		$city_ids = json_encode( __::pluck($data['cities'], 'city_id') );

		//queries
		$data['user'] = $this->Model_user->get( $qp );		

		//init validation
		$this->form_validation->set_rules('firstname', lang('firstname'), 'required');
		$this->form_validation->set_rules('lastname', lang('lastname'), 'required');
		$this->form_validation->set_rules('city_id', lang('city'), 'required|callback_city_check['.$city_ids.']');

		//no validated
		if ($this->form_validation->run() == FALSE)
		{
			//view
			$data['view'] = 'account_profile_edit';
			$data['selected'] ="profile";
			$this->load->view('template', array( 'data' => $data) );
		}
		else //validated
		{
			//update params
			$qp_update = new StdClass;
			$qp_update->where = array(
					"user_id" => $user_id
			);
			$data_update = array (
					'city_id'	=> $this->input->post('city_id'),
					'user_firstname' => $this->input->post('firstname'),
					'user_lastname' => $this->input->post('lastname'),
					'user_contact' => $this->input->post('contact'),
					'user_modified' => date('c')
			);
			
			//newsletter update (city change)
			if ( $this->input->post('city_id') != $data['user']->city_id){
				$this->_mc_list_subscribe( $data['user']->user_email );
			}

			//update query
			$this->Model_user->update ( $qp_update, $data_update );

			//update session
			$new_data['city_id'] = $this->input->post('city_id');
			$new_data['user_firstname'] = $this->input->post('firstname');
			$new_data['user_lastname'] = $this->input->post('lastname');
			$new_data['user_contact'] = $this->input->post('contact');
			$this->session->set_userdata($new_data);

			//alert 
			create_alert( "success", 'alert update profile' );

			//view
			redirect ('account');			
		}	

	


	}

	//-------------------------------------
	private function _mc_list_subscribe ($user_email){
	// newsletter subscription
	//-------------------------------------
		$this->load->library('MailChimp');
		$MailChimp = new MailChimp();
		$result = $MailChimp->call('lists/subscribe', array(
                'id'                => $this->config->item('mc_list_id'),
                'email'             => array('email'=>$user_email),
                'merge_vars'        => array('FNAME'=>$this->input->post('firstname'), 'LNAME'=>$this->input->post('lastname'), 'CITYID' => $this->input->post('city_id')),
                'double_optin'      => false,
                'update_existing'   => true
            ));		
	}

	//REFERRALS
	public function referrals(  )
	{
		//load

		//vars
		$user_id = $this->session->userdata('user_id');

		//view
		$data['view'] = 'account_referrals';
		$data['selected'] ="referrals";
		$this->load->view('template', array( 'data' => $data) );		
	}

	//PASSWORD
	public function password(  )
	{

		if ( !$this->session->userdata('logged_in') )
			redirect ('home');

		//loads
		$this->load->helper('common');
		$this->load->model('Model_user');
		$this->load->library('form_validation');

		//vars
		$user_id = $this->session->userdata('user_id');

		//init validation
		$this->form_validation->set_rules('password', lang('password'), 'required');

		//no validated
		if ($this->form_validation->run() == FALSE)
		{
			//view
			$data['view'] = 'account_password';
			$data['selected'] ="password";
			$this->load->view('template', array( 'data' => $data) );
		}
		else //validated
		{
			$password_hash = $this->simpleloginsecure->hash_this( $this->input->post('password') );

			//update params
			$qp_update = new StdClass;
			$qp_update->where = array(
					"user_id" => $user_id
			);
			$data_update = array (
					'user_pass' => $password_hash,
					'user_modified' => date('c')
			);
			
			//update query
			$this->Model_user->update ( $qp_update, $data_update );

			//alert
			create_alert( "success", 'alert update password' );

			//view
			redirect ('account');			
		}			
	}	
	//LISTINGS
	public function listings (){
		$this->load->model('Model_listing');

		//vars
		$user_id = $this->session->userdata('user_id');

		//params
		$qp = new StdClass;
		$qp->where = array(
			"user_id" => $user_id
		);
		$qp->order = array( 
				"listing_start" => 'DESC'
		);
		$qp->joins = array( "items" => 'items.item_id = listings.item_id');	

		//queries
		$data['listings'] = $this->Model_listing->get( $qp );

		//view
		$data['view'] = 'account_listings';
		$data['selected'] ="listings";
		$this->load->view('template', array( 'data' => $data) );		
	}
	//OFFERS
	public function offers (){
		$this->load->model('Model_offer');

		//vars
		$user_id = $this->session->userdata('user_id');

		//params
		$qp = new StdClass;
		$qp->where = array(
			"user_id" => $user_id
		);
		$qp->order = array( 
				"offer_start" => 'DESC'
		);
		$qp->joins = array( "items" => 'items.item_id = offers.item_id');	

		//queries
		$data['offers'] = $this->Model_offer->get( $qp );

		//view
		$data['view'] = 'account_offers';
		$data['selected'] ="offers";
		$this->load->view('template', array( 'data' => $data) );		
	}
	//PURCHASES
	public function purchases(  )
	{
		//load
		$this->load->model('Model_transaction');

		//vars
		$user_id = $this->session->userdata('user_id');

		//params
		$qp = new StdClass;
		$qp->where = array(
			"user_id" => $user_id
		);
		$qp->order = array( 
				"transaction_date" => 'DESC'
			);	

		//queries
		$data['purchases'] = $this->Model_transaction->get( $qp );

		//view
		$data['view'] = 'account_purchases';
		$data['selected'] ="purchases";
		$this->load->view('template', array( 'data' => $data) );		
	}

	//BUY CREDITS
	public function credits(  )
	{
		//load
		$this->load->helper('common');
		$this->load->model('Model_user');

		//vars
		$user_id = $this->session->userdata('user_id');
		$packages= $this->_get_packages();

		//params
		$qp = new StdClass;
		$qp->where = array(
				"user_id" => $user_id
		);
		$qp->limit = 1;

		//queries
		$data['user'] = $this->Model_user->get( $qp );

		if ( !$this->session->userdata('logged_in') )
			redirect ('home');

		if ( $_POST ){			
			$package = $_POST['package'];
			if ( $packages->$package){
				if ( $this->_do_stripe_transaction( $packages->$package) ){

					//add credits
					$credits = $packages->$package->credits + $packages->$package->free;
					update_credits ( $credits, $data['user'] );

					//create transaction
					$transaction_array = $this->_create_transaction_array ( $packages->$package, $data['user'] );
					$this->_create_transaction ( $transaction_array );

					//send receipt
					$this->_send_receipt_email ($data['user'], $transaction_array );

					//put flashdata
					create_alert( "success", 'alert new credits' );

					//redirect
					redirect('home');

				}
			}	
		}

		$data['view'] = 'account_credits';
		$data['selected'] ="credits";
		$data['packages'] = $packages;
		$this->load->view('template', array( 'data' => $data) );
	}

	//-------------------------------------
	private function _send_receipt_email ( $user, $transaction_array ){
	//-------------------------------------
		require('application/libraries/Mandrill-API/Mandrill.php');

		try {
		    $mandrill = new Mandrill( $this->config->item('mandrill_api_key') );
		    $template_name = 'receipt';
		    $template_content = array();
		    $message = array(
		        'to' => array(
		            array(
		                'email' => $user->user_email,
		                'name' => $user->user_firstname
		            )
		        ),

		        'global_merge_vars' => array(
                    array(
                        'name' => 'TRANSACTIONID',
                        'content' => $transaction_array["transaction_id"]
                    ),
                    array(
                        'name' => 'TRANSACTIONDATE',
                        'content' => date("y/m/d")
                    ),
                    array(
                        'name' => 'TRANSACTIONCREDITS',
                        'content' => $transaction_array["transaction_credits"]
                    ),
                    array(
                        'name' => 'TRANSACTIONPRICE',
                        'content' => $transaction_array["transaction_price"]
                    ),
                    array(
                        'name' => 'FNAME',
                        'content' => $user->user_firstname
                    )          
		        )
		    );
		    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message);

		} catch(Mandrill_Error $e) {
		}

	}

	//-------------------------------------
	private function _create_transaction_array ($package, $user){
	//-------------------------------------
		$credits = $package->credits + $package->free;
		return array(
				"transaction_id" =>	random_string ("unique"),
				"user_id" =>	$user->user_id,
				"transaction_price"	=> $package->price,
				"transaction_credits" => $credits
				);
	}

	//-------------------------------------
	private function _create_transaction ( $transaction_array ){
	//-------------------------------------
		$this->load->model("Model_transaction");
		$this->Model_transaction->create( $transaction_array);		
	}

	//-------------------------------------
	private function _do_stripe_transaction ( $package){
	//-------------------------------------
		//load
		require('application/libraries/lib/Stripe.php');

		//vars
		$result = true;
		$token = $_POST['stripeToken'];

		Stripe::setApiKey($this->config->item('stripe_live_secret_key'));

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
		$charge = Stripe_Charge::create(array(
		  "amount" => $package->price*100, // amount in cents, again
		  "currency" => "usd",
		  "card" => $token,
		  "description" => $this->session->userdata('user_email') )
		);
		} catch(Stripe_CardError $e) {
		  $result = false;
		}
		return $result;
	}

	private function _get_packages(){
		return json_decode( '{
			"4.99":{"credits":5,"free":0,"price":4.99},
			"14.99":{"credits":15,"free":1,"price":14.99},
			"29.99":{"credits":30,"free":3,"price":29.99},
			"44.99":{"credits":45,"free":5,"price":44.99}
		}');		
	}
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */