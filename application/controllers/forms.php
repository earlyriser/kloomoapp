<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forms extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);		
	}

	//-------------------------------------
	public function logout()
	//-------------------------------------
	{
		$this->simpleloginsecure->logout();
		redirect ('home');
	}

	//-------------------------------------
	public function login()
	//-------------------------------------
	{
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');

		//redirect to account if logged
		if ($this->session->userdata('logged_in') )	
			redirect ('home');
		
		//login ok
		if ( $this->simpleloginsecure->login( $email, $password ) )
		{
			redirect ('home');
		}
		else
		{			
			//view
			$data['email'] 	= $email;
			$data['view'] 	= 'login';
			$this->load->view ('template', array('data' => $data)) ;	
		}
	}

	//-------------------------------------
	public function signup ( $referral_id = false )
	//-------------------------------------
	{
		//redirect to account if logged
		if ($this->session->userdata('logged_in') )	
			redirect ('home');

		//load
		$this->load->model('Model_city');
		$this->load->model('Model_user');
		$this->load->library('form_validation');
		$this->load->helper('string');
		$this->load->library('__');		

		//query
		$query_params = new StdClass;
		$data['cities'] = $this->Model_city->get( $query_params );
		$data['referral_id']= $referral_id;
		$city_ids = json_encode( __::pluck($data['cities'], 'city_id') );

		//init validation
		$this->form_validation->set_rules('firstname', lang('firstname'), 'required');
		$this->form_validation->set_rules('lastname', lang('lastname'), 'required');				
		$this->form_validation->set_rules('email', lang('email'), 'required|valid_email|callback_email_check');
		$this->form_validation->set_rules('password', lang('password'), 'required');
		$this->form_validation->set_rules('city_id', lang('city'), 'required|callback_city_check['.$city_ids.']');


		//no validated
		if ($this->form_validation->run() == FALSE)
		{
			$data['view'] 	= 'signup';
			$this->load->view ('template', array('data' => $data)) ;
		}
		else //validated
		{
			$password_hash = $this->simpleloginsecure->hash_this( $this->input->post('password') );
			$user_unique = strtolower ( random_string('alnum', 32 ));

			//create user
			$this->Model_user->create( array ( 
				'user_email' => $this->input->post('email'),
				'city_id'	=> $this->input->post('city_id'),
				'user_firstname' => $this->input->post('firstname'),
				'user_lastname' => $this->input->post('lastname'),
				'user_pass' => $password_hash,
				'user_unique' => $user_unique,
				'user_active' => 0,
				'user_date'	=> date('c'),
				'user_modified' => date('c')
			) );
			$new_user_id =$this->db->insert_id();

			//activation email
			$this->_send_activation_email( $this->input->post('firstname'), $this->input->post('email'), $user_unique);

			//create request if no city
			if ( $this->input->post('city_id') == '0'){
				$this->request_city ();
			}

			//newsletter subscription
			if ( $this->input->post('newsletter') ){
				$this->_mc_list_subscribe ();
			}

			//if referral
			if ( $referral_id ){
				$this->_give_credits($referral_id, $this->config->item('credits_referral') );
			}

			//start credits
			$this->_give_credits( $new_user_id, $this->config->item('credits_start') );

			//redirection
			if ( $this->simpleloginsecure->login( $this->input->post('email'), $this->input->post('password') ) ){
				redirect ('home');
			}

		}	
	}

	//-------------------------------------
	private function _give_credits ( $user_id, $credits){
	// give start credits
	//-------------------------------------
		//loads
		$this->load->helper('common');

		//params
		$qp = new StdClass;
		$qp->where = array( "user_id" => $user_id );
		$qp->limit = 1;
		//query
		$user = $this->Model_user->get( $qp );

		// if user id exists, give credits
		if ( $user ){
			update_credits ( $credits, $user, false  );
		}

	}

	//-------------------------------------
	private function _mc_list_subscribe (){
	// newsletter subscription
	//-------------------------------------
		$this->load->library('MailChimp');
		$MailChimp = new MailChimp();
		$result = $MailChimp->call('lists/subscribe', array(
                'id'                => $this->config->item('mc_list_id'),
                'email'             => array('email'=>$this->input->post('email')),
                'merge_vars'        => array('FNAME'=>$this->input->post('firstname'), 'LNAME'=>$this->input->post('lastname'), 'CITYID' => $this->input->post('city_id')),
                'double_optin'      => true,
                'update_existing'   => true
            ));		
	}

	//-------------------------------------
	private function request_city(){
	// make a request to add a new city
	//-------------------------------------
		$this->load->model('Model_request');

		$this->Model_request->create( array(
			"user_id" => $this->db->insert_id(),
			"request_content" => "city:".$this->input->post('request_city')			
		));

		$alert = new StdClass;
		$alert->type = "warning";
		$alert->content = lang ("new foreign user"); 
		$this->session->set_flashdata('alert', 'value');
	}	
	
	//-------------------------------------
	public function email_check ( $email )
	//-------------------------------------
	{
		//params
		$query_params = new StdClass;
		$query_params->where = array(
				"user_email" => $email
		);

		// username is taken
		if ( $this->Model_user->get( $query_params ) ){
			$this->form_validation->set_message('email_check', 'The email is taken');
			return false;		
		}

		return true;					
	}

	//-------------------------------------
	public function city_check ( $city, $city_ids )
	//-------------------------------------
	{
		$array = json_decode($city_ids);
		array_push($array, 0);

		if ( in_array ( $city, $array) ) return true;
		else{
			$this->form_validation->set_message('city_check', 'Incorrect city');
			return false;
		}					
	}

	//-------------------------------------
	private function _send_activation_email ( $user_firstname, $user_email, $user_unique ){
	//-------------------------------------

		$activation_link = site_url('forms/activation/'.$user_unique);

		require('application/libraries/Mandrill-API/Mandrill.php');

		try {
		    $mandrill = new Mandrill( $this->config->item('mandrill_api_key') );
		    $template_name = 'activation';
		    $template_content = array();
		    $message = array(
		        'to' => array(
		            array(
		                'email' => $user_email,
		                'name' => $user_firstname
		            )
		        ),
		        'global_merge_vars' => array(
                    array(
                        'name' => 'ACTIVATIONLINK',
                        'content' => $activation_link
                    ),
                    array(
                        'name' => 'FNAME',
                        'content' => $user_firstname
                    )          
		        )
		    );
		    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message);

		} catch(Mandrill_Error $e) {
			/*
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
		    throw $e;
		    */
		}

	}

	//-------------------------------------
	public function activation ( $user_unique=false ){
	//-------------------------------------
		//loads
		$this->load->model('Model_user');

		//params
		$qp = new StdClass;
		$qp->where = array("user_unique" => $user_unique);
		$qp->limit = 1; 
		$data['activated'] = false;
		$user = false;

		//query
		if ( $user_unique )
			$user = $this->Model_user->get( $qp ) ;

		//if user exists, activate user
		if ( $user ){
			//update params
			$qp_update = new StdClass;
			$qp_update->where = array( "user_id" => $user->user_id );
			$data_update = array('user_active' => 1);
			//update query
			$this->Model_user->update ( $qp_update, $data_update );
			
			$data['activated'] = 1;
		}		

		$data['view'] 	= 'activation';
		$this->load->view ('template', array('data' => $data)) ;
	}



	//-------------------------------------
	public function forgot_password ()
	//-------------------------------------
	{	
		//loads
		$this->load->model('Model_user');
		$this->load->helper('string');
		$this->load->helper('common');
		
		//vars
		$email 	= $this->input->post('email');
		$user 	= false;
		$token_id = strtolower ( random_string('alpha', 32 ));

		//params
		$qp = new StdClass;
		$qp->where = array("user_email" => $email);
		$qp->limit = 1;

		//query
		if ( $email ){
			$user = $this->Model_user->get( $qp ) ;
		}

		if ( $user )
		{
			//create token
			$this->create_reminder_token ( $user->user_id, $token_id );

			//send email
			$this->_send_email_reminder ( $user->user_firstname, $user->user_email, $token_id );

			//alert
			create_alert( "success", 'alert reminder email' );

			redirect ('forms/login');
		}
		else
		{
			//view
			$data['view'] 	= 'forgot_password';
			$this->load->view ('template', array('data' => $data) );	
		}
	}

	//-------------------------------------
	public function create_reminder_token ( $user_id, $token_id ){
	//create a token in iron.io cache system
	//-------------------------------------	
		//loads
		require("application/libraries/ironio/IronCore.class.php");
		require("application/libraries/ironio/IronCache.class.php");

		$cache = new IronCache('application/libraries/ironio/config.ini');
		$cache->putItem("reminder_tokens", $token_id, array("value" => $user_id,'expires_in' => 60*60*24));
	
	//( $cache->get($token_id) );
		 
	}

	//-------------------------------------
	private function _send_email_reminder ( $user_firstname, $user_email, $token_id ){
	//-------------------------------------

		$reminder_link = site_url('forms/password_reset/'.$token_id);

		require('application/libraries/Mandrill-API/Mandrill.php');

		try {
		    $mandrill = new Mandrill( $this->config->item('mandrill_api_key') );
		    $template_name = 'reminder';
		    $template_content = array();
		    $message = array(
		        'to' => array(
		            array(
		                'email' => $user_email,
		                'name' => $user_firstname
		            )
		        ),
		        'global_merge_vars' => array(
                    array(
                        'name' => 'REMINDERLINK',
                        'content' => $reminder_link
                    ),
                    array(
                        'name' => 'FNAME',
                        'content' => $user_firstname
                    )          
		        )
		    );
		    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message);
		} catch(Mandrill_Error $e) {
			/*
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
		    throw $e;
		    */
		}

	}

	//-------------------------------------
	public function password_reset ( $token_id = false)
	//-------------------------------------
	{
		//load	
		$this->load->model('Model_user');

		//vars
		$user = false;
		
		//loads
		require("application/libraries/ironio/IronCore.class.php");
		require("application/libraries/ironio/IronCache.class.php");

		$cache = new IronCache('application/libraries/ironio/config.ini');

		$cache_item = $cache->getItem("reminder_tokens", $token_id);

		if ( $cache_item ){
			$user_id = $cache_item->value;
			$this->simpleloginsecure->force_login ( $user_id );

			redirect ('account/password');
		}	
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */