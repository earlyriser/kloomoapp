<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listings extends CI_Controller {


	public function __construct()
	{
	    parent::__construct();
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);	    
	    //$this->output->enable_profiler(TRUE);

		//model
		$this->load->model('Model_listing');
		$this->load->model('Model_item');
		$this->load->helper('common');			    
	}

	//------------------------------
	public function delete($item_id){
	//------------------------------
		//not logged
		if ( !$this->session->userdata('logged_in') )
		{	redirect('form/login'); }

		//params
		$qp = new StdClass;
		$qp->where = array(
				"item_id" => $item_id,
				"user_id" => $this->session->userdata('user_id')
		);

		$qp_item = new StdClass;
		$qp_item->where = array("item_id" => $item_id);
		$qp_item->limit = 1;	

		//queries
		$data['item'] = $this->Model_item->get ( $qp_item );

		//if form submitted, unlist
		if ( $_POST ){
			//query
			$data = array ("listing_end" => date('Y-m-d', strtotime("-1 day")) );
			$this->Model_listing->update ( $qp, $data );

			//alert
			create_alert ( "success", "alert deletion listing");
			
			//redirect
			redirect ('home');			
		}

		//view
		$data['view'] = 'form_unlisting';
		$this->load->view('template', array( 'data' => $data) );

	}

	//------------------------------
	private function _prepare_listing( $item_id ){
	//------------------------------
		return
		array(
			'listing_id' => random_string('unique'),
			'user_id' => $this->session->userdata('user_id'),
			'item_id' => $item_id,
			'listing_condition' => $this->input->post('condition'),
			'listing_start' => date("Y-m-d"),
			'listing_end' => date('Y-m-d', strtotime("+1 year"))
		);
	}

	//------------------------------
	public function create($item_id)
	//------------------------------
	{	
		//not logged
		if ( !$this->session->userdata('logged_in') )
		{	redirect('forms/login'); }

		//load
		$this->load->helper('string');
		$this->load->Model('Model_user');
		$this->load->Model('Model_item');

		//vars
		$listing = $this->_prepare_listing( $item_id );

		//params
		$qp = new StdClass;
		$qp->where = array("item_id" => $item_id);
		$qp->limit = 1;
		
		$user_qp = new StdClass;
		$user_qp->where = array("user_id" => $listing['user_id']);
		$user_qp->limit = 1;

		//queries
		$data["item"] = $item = $this->Model_item->get( $qp );
		$user = $this->Model_user->get( $user_qp );

		$data['def_prefix'] = common_get_def_prefix( $data["item"]->category_id);
		
		//if validated, create listing
		if ( common_is_valid_condition ($this->input->post('condition') ) ){
			if ( $this->is_listable( $item_id, $user, $listing['listing_start'] ) ){	
				//insert listing
				$this->Model_listing->create( $listing );
				//credits
				update_credits ( -1, $user );
				//alert
				create_alert ( "success", "alert creation listing");

				redirect ('home');
			}
		}
		
		//view
		$data['view'] = 'form_listing';
		$this->load->view('template', array( 'data' => $data) );		

	}

	//------------------------------
	private function is_listable( $item_id, $user, $current_time ){
	//------------------------------
		$result = false;
		$has_credits = has_credits( $user );

		if ( !$has_credits)
			redirect('account/credits');
					
		if ( $has_credits AND 
			 $this->is_unique_item( $item_id, $user->user_id, $current_time ) )
		{
			$result = true;
		}

		return $result;
	}

	private function is_unique_item( $item_id, $user_id, $current_time ){

		$query_params = new StdClass;
		$query_params->where = array( 
				"listing_end >=" => $current_time,
				"item_id" => $item_id,
				"user_id" => $user_id
			);
		$query_params->limit = 1;
		$query_params->order = array( "item_id" => 'ASC');

		if ( $this->Model_listing->get( $query_params ) )
			return false;

		return true;
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */


