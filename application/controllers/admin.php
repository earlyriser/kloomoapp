<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);

		//model
		$this->load->model('Model_category');
		$this->load->model('Model_country');
		$this->load->model('Model_item');
		$this->load->model('Model_offer');
		$this->load->model('Model_listing');
		$this->load->model('Model_user');
		$this->load->model('Model_item_country');
		$this->load->helper('common');

		if (  !($this->session->userdata('user_email') == 'romama@gmail.com' AND
			$this->session->userdata('logged_in') == 'romama@gmail.com' AND
			$this->session->userdata('user_id') == '1' ))
		{
			redirect ('home');
		}

	}

	public function cities(){

		//vars
		$end = date("Y-m-d");

		$qp_users = new StdClass;
		$qp_users->order = array ("city_id" => 'DESC');

		$qp_listings = new StdClass;
		$qp_listings->where = array( "listing_end >=" => $end);	
		$qp_listings->joins = array( 
			"users" => 'users.user_id = listings.user_id'
		);

		$qp_offers = new StdClass;
		$qp_offers->where = array( "offer_end >=" => $end);	
		$qp_offers->joins = array( 
			"users" => 'users.user_id = offers.user_id'
		);		


		$cities = array();
		$users = $this->Model_user->get ( $qp_users);
		$listings = $this->Model_listing->get ( $qp_listings);
		$offers = $this->Model_offer->get ( $qp_offers);

		foreach ( $users as $user){
			if ( !array_key_exists ( $user->city_id, $cities )){
				$cities[$user->city_id] = array();
				$cities[$user->city_id]['users']=array();
				$cities[$user->city_id]['listings']=array();
				$cities[$user->city_id]['offers']=array();
			}
			array_push( $cities[$user->city_id]['users'], $user);
		}

		if ( $listings)
		foreach ( $listings as $listing){
			array_push( $cities[$listing->city_id]['listings'], $listing);
		}

		if ( $offers)
		foreach ( $offers as $offer){
			array_push( $cities[$offer->city_id]['offers'], $offer);
		}		

		//view
		$data['view'] 	= 'admin_dashboard_cities';
		$data['cities'] = $cities;
		$this->load->view ('template', array( 'data' => $data));

	}

	public function items ()
	{
		//$this->output->enable_profiler(TRUE);

		//queries
		$data['categories'] = $this->Model_category->get ( $this->_set_categories_query ());
		$data['items'] = $this->Model_item->get ( $this->_set_items_query ());
		$data['countries'] = $this->Model_country->get ( $this->_set_countries_query ());
		$data['items_countries'] = $this->Model_item_country->get ( $this->_set_items_countries_query ());

		//view
		$data['view'] 	= 'admin_items';
		$this->load->view ('template', array( 'data' => $data));					
	}

	public function dashboard_30(){

		//vars
		$end = date("Y-m-d");
		$start = date('Y-m-d', strtotime("-30 days"));

		//params
		$qp_offers = new StdClass;
		$qp_offers->where = array( "offer_start >="	=> $start, "offer_start <=" => $end);
		$qp_offers->joins = array( 
			"users" => 'users.user_id = offers.user_id',
			"items" => 'items.item_id = offers.item_id'
		);
		$qp_offers->order = array ("offer_start" => 'DESC');

		$qp_listings = new StdClass;
		$qp_listings->where = array( "listing_start >="	=> $start, "listing_start <=" => $end);
		$qp_listings->joins = array( 
			"users" => 'users.user_id = listings.user_id',
			"items" => 'items.item_id = listings.item_id'
		);
		$qp_listings->order = array ("listing_start" => 'DESC',);

		$qp_users = new StdClass;
		$qp_users->where = array( "user_date >=" => $start, "user_date <=" => $end );

		//queries
		$data['offers'] = $this->Model_offer->get ( $qp_offers);
		$data['listings'] = $this->Model_listing->get ( $qp_listings);
		$data['users'] = $this->Model_user->get ( $qp_users);



		//view
		$data['view'] 	= 'admin_dashboard_30';
		$this->load->view ('template', array( 'data' => $data));


		
	}

	/* ADD ITEM  form action*/
	public function add_item(){
		//load
		$this->load->model('Model_item');

		//vars
		$item_id = false;
		$categories=array(
		1=>1000,	 //tablets
		3=>12000, //ps3
		4=>11000, //x360
		5=>10000, //wii
		6=>13000, //ps4
		7=>14000, //xone
		8=>15000   //wiiu
		);

		//if validates, create it
		if ( $this->input->post('item_name') AND $this->input->post('category_id') ){
			//get item_id
			$qp_items = new StdClass;
			$qp_items->order = array("item_id" => "DESC");
			$qp_items->where = array("category_id" => $this->input->post('category_id') );
			$items = $this->Model_item->get ( $qp_items);
			if ( $items ){
				$item_id = $items[0]->item_id+1;
			}else{
				$item_id= $categories[$this->input->post('category_id')];
			}
		}

		//insert item
		if ( $item_id){
			$item = array ( 
				"item_id" => $item_id,
				"item_name" => $this->input->post('item_name'),
				"category_id" => $this->input->post('category_id')
			);

			$this->Model_item->create ( $item );

			create_alert ( "success", "alert creation item");
		}else{
			create_alert ( "danger", "alert error");
		}

		redirect ('admin/items');		
	}	

	/* ITEMS BY COUNTRY  form action*/
	public function items_by_country(){
		$this->load->model('Model_item_country');
		$items_countries = $this->Model_item_country->get ( $this->_set_items_countries_query ());

		if ( $_POST ){

			//loop to detect new inserts
			foreach ( $_POST as $input => $val){
				$checkbox = new StdClass;
				$params = explode("/", $input);
				$checkbox->country_id = $params[0];
				$checkbox->item_id = $params[1];
				$new = true;

				foreach ( $items_countries as $item_country){
					if ( ($checkbox->item_id == $item_country->item_id && 
						  $checkbox->country_id ==	$item_country->country_id ) ){
						$new = false;
					}
				}

				if ($new){
					$this->Model_item_country->create( array('item_id'=> $checkbox->item_id, 'country_id' => $checkbox->country_id));
				}
			}


			//loop to delete
			foreach ( $items_countries as $item_country){
				$delete = true;
				foreach ( $_POST as $input => $val){

					$checkbox = new StdClass;
					$params = explode("/", $input);
					$checkbox->country_id = $params[0];
					$checkbox->item_id = $params[1];

					if ( ($checkbox->item_id == $item_country->item_id && 
						  $checkbox->country_id ==	$item_country->country_id ) ){
						$delete = false;
					}
				}

				if ( $delete ){
					$this->Model_item_country->delete ( array( 'country_id' => $item_country->country_id, 'item_id' => $item_country->item_id ));
				}
			}

		}

		redirect ('admin/items');
	}

	/* DELETE ITEM  form action*/
	public function delete_item(){

		//load
		$this->load->model('Model_item');
		$this->load->helper('common');

		//delettion
		$this->Model_item->delete ( 'item_id', $this->input->post('item_id') );
		create_alert ( "success", "alert deletion item");

		redirect ('admin/items');
	}

	/* CATEGORIES */
	private function _set_categories_query( ){
		$query_params = new StdClass;
		$query_params->order = array( "category_name" => "ASC");
		return $query_params;
	}

	/* COUNTRIES */
	private function _set_countries_query( ){
		$query_params = new StdClass;
		$query_params->order = array( "country_name" => "ASC");
		return $query_params;
	}

	/* ITEMS */
	private function _set_items_query( ){
		$query_params = new StdClass;
		$query_params->order = array( "category_id"=>"ASC", "item_name" => "ASC");
		return $query_params;
	}

	/* ITEMS COUNTRIES*/
	private function _set_items_countries_query( ){
		$query_params = new StdClass;
		$query_params->order = array( "item_id" => "ASC");
		return $query_params;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */


