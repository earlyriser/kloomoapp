<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mailing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(TRUE);

		$this->mylang = get_lang();
		$this->lang->load('email', $this->mylang);	

		
		if (  !($this->session->userdata('user_email') == 'romama@gmail.com' AND
			$this->session->userdata('logged_in') == 'romama@gmail.com' AND
			$this->session->userdata('user_id') == '1' ))
		{
			redirect ('home');
		}			
	}

	/* FUNCTIONS
	_create_newsletter_content
	mc_lists_subscribe
	mc_folders_list
	mc_campaigns_create
	mc_campaigns_segment_test
	*/

	//-------------------------------------
	private function _create_newsletter_content ( $city ){
	//-------------------------------------

		//loads
		$this->load->helper('common');
		
		//queries
		$city_object = get_city_object( $city->city_id ) ;

		$str = "*|FNAME|*, ".lang('here are...').$city->city_name.".\r\n".lang('go to kloomo...')."\r\n";

		foreach ( $city_object->categories as $category_webname => $category){
			$str1 = "";
			$str1 .= "\r\n";
			$str1 .= strtoupper ($category_webname);
			$str1 .= "\r\n==============================================\r\n";

			foreach ($category->items as $item) { 
				$str2="";
				if ( $item->listings->counter > 0 ||
					 $item->offers->counter > 0 ){
					$min = $max = 0;


					if ( $item->offers->conditions['i']->min > $min){
						$min = $item->offers->conditions['i']->min;}
					if ( $item->offers->conditions['ii']->min > $min){
						$min = $item->offers->conditions['ii']->min;}
					if ( $item->offers->conditions['iii']->min > $min){
						$min = $item->offers->conditions['iii']->min;}					

					$max = $min;

					if ( $item->offers->conditions['i']->max > $max){
						$max = $item->offers->conditions['i']->max;}
					if ( $item->offers->conditions['ii']->max > $max){
						$max = $item->offers->conditions['ii']->max;}
					if ( $item->offers->conditions['iii']->min > $max){
						$max = $item->offers->conditions['iii']->max;}

					$str2 .= $item->item_name." -----------> ".$item->listings->counter." ".lang('listings')." & ".$item->offers->counter." ".lang('offers')." ";
					if ( $min ){
						$str2 .= " ".lang('from').": ".$min." ".lang('to')." ".$max;}
					$str2 .= "\r\n"; 
				}
				$str1.=$str2;
			}
			$str .= $str1;
		}
		echo $str;
		return $str;
	}

	//-------------------------------------
	public function mc_lists_subscribe(){
	//-------------------------------------

		$email ="romama@gmail.com";
		$firstname = "Roberto";
		$lastname = "Martinez";
		$city_id = "new_york";


		$this->load->library('MailChimp');
		$MailChimp = new MailChimp();
		$result = $MailChimp->call('lists/subscribe', array(
                'id'                => '1235639784',
                'email'             => array('email'=>'romama@gmail.com'),
                'merge_vars'        => array('FNAME'=>'Davy', 'LNAME'=>'Jones'),
                'double_optin'      => false,
                'update_existing'   => true,
                'replace_interests' => false,
                'send_welcome'      => false,
            ));
		print_r($result);
	}


	//-------------------------------------
	public function mc_folders_list(){
	//-------------------------------------
		$this->load->library('MailChimp');
		$MailChimp = new MailChimp();
		$result = $MailChimp->call('folders/list', array(
			'type' => 'campaign'
		));

		print_r( $result);
	}

	//-------------------------------------
	public function mc_lists_list(){
	//-------------------------------------
		$this->load->library('MailChimp');
		$MailChimp = new MailChimp();
		$result = $MailChimp->call('lists/list');

		print_r( $result);
	}	

	//-------------------------------------
	public function mc_campaigns_create( $city_id ){
	//-------------------------------------

		//load
		$this->load->model('Model_city');

		//queries
		$qp = new StdClass;
		$qp->where = array( "city_id" => $city_id);
		$qp->limit = 1;		
		$city = $this->Model_city->get( $qp );

		$content_text = $this->_create_newsletter_content ( $city );
		$list_id	= $this->config->item('mc_list_id');
		$subject	= lang('subject')." ".$city->city_name;
		$from_name = "Kloomo";
		$from_email = "contact@kloomo.com";

		$this->load->library('MailChimp');
		$MailChimp = new MailChimp();
		$result = $MailChimp->call('campaigns/create', array(		
            'type'          	=> 'regular',
            'options'         	=> array(
            	'list_id'	=> $list_id,
            	'subject'	=> $subject,
            	'from_name'=> $from_name,
            	'from_email'=> $from_email           	
            ),
            'content'        	=> array( 
            	'text'		=> $content_text
            ),
            'segment_opts'      => array(
            	'match'		=> 'all',
            	'conditions'	=> array(
            		array(
            			'field' => 'CITYID',
            			'op'	=> 'eq',
            			'value' => $city_id
            		)
            	)
            ),
            'type_opts'   		=> array()
        ));

        print_r( $result );			
	}

	//-------------------------------------
	public function mc_campaigns_segment_test(){
	//-------------------------------------

		$list_id	= $this->config->item('mc_list_id');
		$city_id = 'new_york';

		$this->load->library('MailChimp');
		$MailChimp = new MailChimp();
		$result = $MailChimp->call('campaigns/segment-test', array(		
            'list_id'	=> $list_id,
            'options'      => array(
            	'match'		=> 'all',
            	'conditions'	=> array(
            		array(
            			'field' => 'CITYID',
            			'op'	=> 'eq',
            			'value' => $city_id
            		)
            	)
            )
        ) );		

		print_r( $result );
	}


}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */


