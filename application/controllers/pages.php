<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->mylang = get_lang();
		$this->lang->load('general', $this->mylang);
		$this->lang->load('pages', $this->mylang);
	}

	public function howto(  )
	{
		//queries
		$data['view'] = 'pages_howto';
		$this->load->view('template', array( 'data' => $data) );
	}


	public function about(  )
	{
		//queries
		$data['view'] = 'pages_about';
		$this->load->view('template', array( 'data' => $data) );
	}

	public function tos(  )
	{
		//queries
		$data['view'] = 'pages_tos';
		$this->load->view('template', array( 'data' => $data) );
	}

	public function contact(  )
	{
		//queries
		$data['view'] = 'pages_contact';
		$this->load->view('template', array( 'data' => $data) );
	}		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */