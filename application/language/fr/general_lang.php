<?php

//months
$lang['month_01']="Janvier";
$lang['month_02']="Février";
$lang['month_03']="Mars";
$lang['month_04']="Avril";
$lang['month_05']="Mai";
$lang['month_06']="Juin";
$lang['month_07']="Juillet";
$lang['month_08']="Août";
$lang['month_09']="Septembre";
$lang['month_10']="Octobre";
$lang['month_11']="Novembre";
$lang['month_12']="Décembre";

//footer
$lang['contact_us'] = "Nous joindre";
$lang['privacy'] = "Confidentialité";
$lang['tos'] = "Conditions d'utilisation";

//header
$lang['dashboard'] = "Tableau de bord";
$lang['questions'] = "Questions";
$lang['logout'] = "Déconnexion";
$lang['signup'] = "Inscription";
$lang['login'] = "Connexion";

$lang['current_question'] = "Question actuelle";
$lang['plans'] = "Plans";
$lang['settings'] = "Paramètres";

//home
$lang['home_title'] = "Mesurez la satisfaction de vos clients";
$lang['home_subtitle'] = "Collectez des commentaires anonymes et honnêtes<br>pour améliorer votre chiffre d'affaires !<br><br>Idéal pour restaurants, événements et hôtels.";
$lang['home_cta'] = "Essai gratuit. Aucune carte de crédit requise.";
$lang['how_work'] = "Comment ça fonctionne?";
$lang['step_1'] = "Maintenant vos clients n'ont qu'à effleurer le pad avec leur téléphone intelligent";
$lang['step_2'] = "Pour vous laisser un commentaire rapide en moins de 5 secs. 100% confidentiel";
$lang['step_3'] = "Que vous pourrez analyser facilement";
$lang['pricing'] = "TARIFS";
$lang['basic'] = "De base";
$lang['pro'] = "Professionnel";
$lang['business'] = "Corporatif";
$lang['free'] = "Gratuit";
$lang['unlimited'] = "Illimité";
$lang['customer_feedback'] = "Commentaires de clients";
$lang['receive_kit'] = "Réception de la trousse d'intro gratuite Satisfacture";
$lang['qty_pad'] = "Nb. de pads"; 
$lang['web_analytics_dashboard'] = "Panneau d'accès aux statistiques";
$lang['email_support'] = "Support par courriel";
$lang['phone_support'] = "Support téléphonique";
$lang['create_your_look'] = "Personnalisez votre page";
$lang['creat_your_own_questions'] = "Créez vos propres questions et personnalisez votre pad";
$lang['call_us'] = "Appelez-nous";

//login
$lang['email'] = "Courriel";
$lang['password'] = "Mot de passe";
$lang['password_reminder'] = "Mot de passe oublié";

//create question
$lang['create_question'] = "Créer une question";
$lang['type_of_question'] = "Type de question";
$lang['question_textual'] = "Texte";
$lang['question_optional'] = "Choix multiples";
$lang['question'] = "Question";
$lang['option'] = "Choix";
$lang['add_new_option'] = "Ajouter un choix";

//question manage
$lang['report'] = "Rapport";
$lang['view'] = "Aperçu";
$lang['make_current_question'] = "En faire ma question actuelle";
$lang['current_question_changed'] = "Votre nouvelle question actuelle est activée";

//question report
$lang['answers_to_your_question'] = "Réponses à vos questions";
$lang['no_answers'] = "Il n'y a pas de réponse à cette question pour l'instant";

//question show
$lang['thanks_msg'] = "Merci pour votre commentaire";
$lang['submit'] = "Soumettre";
$lang['comment'] = "Commentaire";
$lang['powered'] = "Propulsé par";

//signup
$lang['username'] = "Nom d'utilisateur";
$lang['enterprise'] = "Entreprise";
$lang['address'] = "Adresse";
$lang['state'] = "État/Province";
$lang['phone'] = "Téléphone";
$lang['contact_person'] = "Nom de la personne contact";

//pin
$lang['welcome_to_satisfacture'] = "Bienvenue sur Satisfacture";
$lang['your_kit_soon'] ="Vous recevrez votre trousse de départ bientôt";
$lang['enter_pin'] = 'Entrez le NIP que vous avez reçu';
$lang['pin_good'] = "Votre trousse a été activée. Veuillez sélectionner votre question actuelle dans la liste ci-dessous ou créer une nouvelle question.";
$lang['pin_bad'] = "NIP erroné. Essayez à nouveau.";

//settings
$lang['settings'] = "Paramètres";
$lang['background_color'] = "Couleur de l'arrière plan";
$lang['title_color'] = "Couleur du titre";
$lang['option_color'] = "Couleur du choix";
$lang['text_color'] = "Couleur du texte";
$lang['button_color'] = "Couleur du bouton";
$lang['edit_settings'] = "Éditer paramètres";
$lang['settings_changed'] = "Vos paramètres ont été changés";

//password reminder
$lang['msg_password_sent'] = "Un message pour récupérer votre mot de passe vous sera envoyé sous peu. Veuillez vérifier votre boîte de réception.";
$lang['msg_login_url'] = "Veuillez changer votre mot de passe.";

//settings_password
$lang['password_changed'] = "Votre mot de passe a été changé.";
$lang['change_password'] = "Changer mot de passe";

$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";


