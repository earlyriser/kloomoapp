<?php

//NEWSLETTER
$lang['here are...'] = "here are the current listings and offers in ";
$lang['go to kloomo...'] = "Go to Kloomo to accept one of them or make a new one.";
$lang['listings'] = "Listings";
$lang['offers'] = "Offers";
$lang['from'] = "from";
$lang['to'] = "to";
$lang['subject'] = "Kloomo Newsletter | New listings & offers in";
$lang[''] = "";
$lang[''] = "";


//PASSWORD 
$lang['email_password_subject'] = "Satisfacture Password reset"; 

$lang['email_password_content']		= <<<EOM
You're receiving this email because you requested a password reset for your user account at Satisfacture.

Please go to the following page and choose a new password:
**LINK**

Best regards,
Satisfacture team
EOM;
//-------------------------------


//CONFIRMATION
$lang['email_confirmation_subject'] = 'Satisfacture Confirmation email';

$lang['email_confirmation_content']	 = <<<EOM
Welcome to Satisfacture!
	
Visit this link to confirm that you own this email address:
**LINK**
	
If you've received this email by mistake, simply delete it.
	
Best regards,
Satisfacture team
EOM;
//------------------------------



//PRO SUBSCRIBE
$lang['email_pro_subscribe_subject'] = 'Your Qranberry Pro Account';

$lang['email_pro_subscribe_content'] = <<<EOM

Hi there!

Your new Pro account is ready.

There are 3 things you may want to do in your Account Panel:
1. Select a username to make your site accessible from qranberry.me/username
2. Set a homepage from all the pages on your site
3. Choose a theme

Feel free to email us if you have any trouble.

Best regards,
The Qranberry team
EOM;

//--------------------------------




//WELCOME
$lang['email_pro_subscribe_subject'] = 'Welcome to Qranberry';

$lang['email_pro_subscribe_content'] = <<<EOM

Hi there!

I noticed that you have recently signed up for a Qranberry account. I want to welcome you to our app and hope that you will find Qranberry useful.

If you have any questions, suggestions, bugs, or bliss using this app, I'm here to hear them!

Cheers,
Roberto Martinez
Founder
EOM;

//--------------------------------


$lang['email_plan_unsubscribe'] ="-";

/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */