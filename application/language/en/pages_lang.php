<?php

$lang['contact page'] ="Send your questions & comments to:<br> contact [at] kloomo [dot] com";
$lang['tos page'] ="
<h2>Kloomo is a Venue</h2>
<p>
Kloomo acts as a venue to allow users, who comply with Kloomo policies to find, offer, sell and/or buy certain products. Kloomo is not directly involved in the transaction between buyers and sellers. As a result, Kloomo has no control over the quality, safety, or legality of any aspect of the items listed the truth or accuracy of the listings, the ability of sellers to sell items or the ability of buyers to pay for items. Kloomo does not pre-screen users or the content or information provided by users. Kloomo cannot ensure that a buyer or seller will actually complete a transaction. Kloomo encourages you to communicate directly with potential transaction partners through the tools available on the Site.
</p>

<h2>Membership Eligibility</h2>
<p>
You represent and warrant that you are at least 18 years old and that all registration information you submit is accurate and truthful. Kloomo may, in its sole discretion, refuse to offer access to or use of the Site to any person or entity and change its eligibility criteria at any time. This provision is void where prohibited by law and the right to access the Site is revoked in such jurisdictions.
Individuals under the age of 18 must at all times use Kloomo services only in conjunction with and under the supervision of a parent or legal guardian who is at least 18 years of age. In this all cases, the adult is the user and is responsible for any and all activities.

<br>
Password: Keep your password secure. You are fully responsible for all activity, liability and damage resulting from your failure to maintain password confidentiality. 
<br>
Account Information: You must keep your account information up-to-date and accurate at all times, including a valid email address.
<br>
Right to Refuse Service: Kloomo services are not available to temporarily or indefinitely suspended Kloomo members. Kloomo reserves the right, in Kloomo sole discretion, to cancel unconfirmed or inactive accounts. Kloomo reserves the right to refuse service to anyone, for any reason, at any time. 
</p>

<h2>Listing and Selling</h2>
<p>
Listing Description: By listing an item on the Site you warrant that you and all aspects of the item comply with Kloomo published policies. You also warrant that you may legally sell the item. 
</p>


<h2>User communications, transactions and disputes</h2>
<p>
Kloomo and Kloomo Representatives are not parties to, have no involvement or interest in, make no representations or warranties as to, and have no responsibility or liability with respect to any communications, transactions, interactions, disputes or any relations whatsoever between you and any other user. 
You agree to hold Kloomo harmless from and against any third-party claim, cause of action, demand or damages related to or arising out of your interactions with others. 
</p>


<h2>No Warranty</h2>
<p>
KLOOMO, KLOOMO'S SUBSIDIARIES, OFFICERS, DIRECTORS, EMPLOYEES, AND KLOOMO'S SUPPLIERS PROVIDE KLOOMO'S WEB SITE AND SERVICES \"AS IS\" AND WITHOUT ANY WARRANTY OR CONDITION, EXPRESS, IMPLIED OR STATUTORY. KLOOMO, KLOOMO'S SUBSIDIARIES, OFFICERS, DIRECTORS, EMPLOYEES AND KLOOMO'S SUPPLIERS SPECIFICALLY DISCLAIM ANY IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, PERFORMANCE, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN ADDITION, NO ADVICE OR INFORMATION (ORAL OR WRITTEN) OBTAINED BY YOU FROM KLOOMO SHALL CREATE ANY WARRANTY. SOME STATES DO NOT ALLOW THE DISCLAIMER OF IMPLIED WARRANTIES, SO THE FOREGOING DISCLAIMER MAY NOT APPLY TO YOU. THIS WARRANTY GIVES YOU SPECIFIC LEGAL RIGHTS AND YOU MAY ALSO HAVE OTHER LEGAL RIGHTS THAT VARY FROM STATE TO STATE. 
</p>

<h2>Liability Limit</h2>
<p>
IN NO EVENT SHALL KLOOMO, AND (AS APPLICABLE) KLOOMO'S SUBSIDIARIES, OFFICERS, DIRECTORS, EMPLOYEES OR KLOOMO'S SUPPLIERS BE LIABLE FOR ANY DAMAGES WHATSOEVER, WHETHER DIRECT, INDIRECT, GENERAL, SPECIAL, COMPENSATORY, CONSEQUENTIAL, AND/OR INCIDENTAL, ARISING OUT OF OR RELATING TO THE CONDUCT OF YOU OR ANYONE ELSE IN CONNECTION WITH THE USE OF THE SITE, KLOOMO'S SERVICES, OR THIS AGREEMENT, INCLUDING WITHOUT LIMITATION, LOST PROFITS, BODILY INJURY, EMOTIONAL DISTRESS, OR ANY SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES. 
KLOOMO'S LIABILITY, AND (AS APPLICABLE) THE LIABILITY OF KLOOMO'S SUBSIDIARIES, OFFICERS, DIRECTORS, EMPLOYEES, AND SUPPLIERS, TO YOU OR ANY THIRD PARTIES IN ANY CIRCUMSTANCE IS LIMITED TO THE GREATER OF (A) THE AMOUNT OF FEES YOU PAY TO KLOOMO IN THE 12 MONTHS PRIOR TO THE ACTION GIVING RISE TO LIABILITY.
</p>

<h2>Indemnity </h2>
<p>
YOU AGREE TO INDEMNIFY AND HOLD KLOOMO AND (AS APPLICABLE) KLOOMO'S PARENT, SUBSIDIARIES, AFFILIATES, OFFICERS, DIRECTORS, AGENTS, AND EMPLOYEES, HARMLESS FROM ANY CLAIM OR DEMAND, INCLUDING REASONABLE ATTORNEYS' FEES, MADE BY ANY THIRD PARTY DUE TO OR ARISING OUT OF YOUR BREACH OF THIS AGREEMENT OR THE DOCUMENTS IT INCORPORATES BY REFERENCE, OR YOUR VIOLATION OF ANY LAW OR THE RIGHTS OF A THIRD PARTY. 
</p>

<h2>No Guarantee</h2>
<p>
Kloomo does not guarantee continuous, uninterrupted access to the Site, and operation of the Site may be interfered with by numerous factors outside Kloomo control. 
</p>

<h2>Legal Compliance; Taxes </h2>
<p>
You shall comply with all applicable domestic and international laws, statutes, ordinances and regulations regarding your use of the Site and any Kloomo service and, if applicable, your listing, purchase, solicitation of offers to purchase, and sale of items. In addition, you shall be responsible for paying any and all taxes applicable to any purchases or sales of items you make on the Site (excluding any taxes on Kloomo net income). 
</p>


<h2>Service</h2>
<p>
Kloomo reserves the right to modify or terminate the Kloomo service for any reason, without notice, at any time. Kloomo reserves the right to alter these Terms of Use or other Site policies at any time, so please review the policies frequently. If Kloomo makes a material change Kloomo will notify you here.
</p>";

$lang['about page'] = "<br><h2>It's blazing fast</h2>
			<p>
			Listing an item or making an offer just take some seconds. No description to enter or picture to upload, just a couple of clicks.
			</p>

			<h2>But you can take your time</h2>
			<p>
			Listings can stay up to a year, then you're not pressed to take a low offer just because your time window is ending.
			</p>

			<h2>It makes you money</h2>
			<p>
			Electronics and entertainment products lose value everyday, you can list them here and sell them before their value continues dropping.
			</p>

			<h2>It saves you money</h2>
			<p>
			Make the offer you want and respect your budget. If it's not accepted, just relax and repost your offer, eventually (as the prices go down) it will be accepted.
			</p>

			<h2>Good for the planet</h2>
			<p>
			When you buy pre-owned products, you are saving resources and reducing the impacts of pollution.
			</p>

			<hr>

			<p>
			Kloomo was founded in 2013 by <a href='http://twitter.com/earlyriser'>Roberto Martinez</a>, who wanted to buy some used tablets and games but couldn't find an easy alternative.
			</p>
			";

/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */