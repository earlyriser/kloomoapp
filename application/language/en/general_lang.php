<?php

$lang['meta description']="Kloomo is the marketplace for popular pre-owned products. Just best sellers, tiny fees, express listing.";
//signup
$lang['signup'] = "Sign up";
$lang['firstname'] = "First name";
$lang['lastname'] = "Last name";
$lang['email'] = "Email";
$lang['password'] = "Password";
$lang['city'] = "City";
$lang['other city'] = "Other city";
$lang['your city'] = "Your city";
$lang['create account'] = "Create account";
$lang['alert creation foreign user'] = "Great! Your account has been created. But we don't have a marketplace for you city. We will notify you as soon we have one.";
$lang['i'] = "Usable";
$lang['ii'] = "Good";
$lang['iii'] = "Excellent";
$lang['change location'] = "Change location";
$lang['receive newsletter'] = "Receive weekly newsletter";
$lang['newsletter warning'] = "Are you sure? You could miss a sweet deal: it contains the new listings & offers in your city.";

//activation
$lang['account activation'] = "Account Activation";
$lang['activation ok'] = "Your account has been activated. Thanks!";
$lang['activation error'] = "Oops! It seems this account does not longer exist. Please sign up again.";

//login
$lang['login'] = "Login";
$lang['forgot password'] = "Forgot your password?";
$lang['reset password'] = "Reset password";

//forgot password
$lang['reset password instructions'] = "We'll email you instructions on how to reset your password.";
$lang['alert reminder email'] = "Instructions to reset your password have been sent to your email.";

//password
$lang['alert update password'] = "Your password has been reset";

//menu
$lang['how it works'] = "How it works";
$lang['account'] = "Account";

//home
$lang['logout'] = "Logout";
$lang['alert creation listing'] = "Your listing has been published";
$lang['alert creation offer'] = "Your offer has been published";
$lang["alert deletion listing"] = "Your listing has been deleted";
$lang["alert deletion offer"] = "Your offer has been retired";
$lang['alert no city'] = "Please select your city. If it's not there, please check back later, we're adding more locations";
$lang['alert no credits'] = "Oops, you don't have enough credits.";
$lang['alert no activity'] = "<b>Hello *firstname*,</b><br>It seems you're just starting. <br>Click on the \"have\" buttons to list the stuff you have & receive offers for it.<br>Click on the \"want\" buttons to make offers for the stuff you want.";
$lang['alert no contact'] = "You made an offer, but there's no way for the sellers to contact you. Please update your <a href='*URL_ADD*'>CONTACT INFO</a>";
$lang["home tagline"] = "The marketplace for popular pre-owned products"; //The marketplace for best-selling products
$lang["home sell"] = "Sell the stuff you have";
$lang["sell p1"] = "Listing takes 5 seconds";
$lang["sell p2"] = "1$ listing fee";
$lang["home buy"] = "Buy at the price you want";
$lang["buy p1"] = "Make your offer";
$lang["buy p2"] = "Local pickup";
$lang["home cta"] = "Get 5 credits<br> SIGN UP now";
$lang['category_1'] = "Tablets";
$lang['category_2'] = "Phones";
$lang['category_3'] = "PS3";
$lang['category_4'] = "XBox 360";
$lang['category_5'] = "Wii";
$lang['category_6'] = "PS4";
$lang['category_7'] = "XBox One";
$lang['category_8'] = "Wii U";
$lang['create your listing'] = "Create your listing";
$lang['delete your listing'] = "Delete your listing";
$lang['make your offer'] = "Make your offer";
$lang['show mine'] ="Only mine";
$lang['show current'] ="Current listings/offers";

$lang["games iii"]="<li>Game plays perfectly.</li><li>Disc without scratches.</li><li>Case without scratches.</li><li>No damage to game instructions.</li>";
$lang["games ii"]="<li>Game plays perfectly.</li><li>Disc may have scratches.</li><li>Case may have wear, but no cracks or damage.</li><li>Instructions may not be present.</li>";
$lang["games i"]="<li>Game plays perfectly.</li><li>Case and instructions, if present, show heavy use.</li>";
$lang["tabletphones iii"]="<li>Device turns on.</li><li>Display has no damaged pixels.</li><li>Device holds charge for multiple hours.</li><li>All buttons work properly.</li><li>Screen & body without scratches.</li><li>Box and instructions included.</li>";
$lang["tabletphones ii"]="<li>Device turns on.</li><li>Display has no damaged pixels.</li><li>Device holds charge for multiple hours.</li><li>All buttons work properly.</li><li>Screen & body may have daily wear scratches.</li><li>Box and instructions may not be present.</li>";
$lang["tabletphones i"]="<li>Device turns on.</li><li>Display may have some damaged pixels.</li><li>All buttons work properly.</li><li>Screen & body maye have daily wear scratches.</li><li>Box and instructions may not be present.</li><li>Cracks may be present.</li>";

//account
$lang['dashboard'] = 'Dashboard';
$lang['credits'] = "Credits";
$lang['purchases'] = "Purchases";
$lang['profile'] = "Profile";
$lang['edit profile'] = "Edit profile";
$lang['update profile'] ="Update profile";
$lang['contact details'] = "Contact details";
$lang['view'] = "View";
$lang['edit'] = "Edit";
$lang['name'] = "Name";
$lang['remaining credits']= "Remaining credits";
$lang['alert update profile'] = "Your profile has been updated";
$lang['listings'] = "Listings";
$lang['offers'] = "Offers";
$lang['0 listings'] ="No listings in your history";
$lang['0 offers'] = "No offers in your history";
$lang['only visible sellers'] = "Only visible for sellers receiving your offers";
$lang['public'] = "Public";
$lang['private'] = "Private";
$lang['contact example'] = "Example: #555 5555 from 5pm-10pm or jsmith[at]smyemail.com";


//credits
$lang['buy credits'] = "Buy credits";
$lang['alert new credits'] = "Great! Your new credits have been added.";
$lang['processing payment']="Processing payment. Please wait.";

//referrals
$lang['get free credits'] ="Get free credits";
$lang['referral 1'] = "For every friend who joins and confirms his/her signup, we'll give you";
$lang['referral 2'] = "extra credits";
$lang['referral 3'] = "Copy this link and share it with your friends.";

//purchases
$lang['date'] = "Date";
$lang['amount'] = "Amount";
$lang['id'] = "Id";
$lang['0 purchases'] = "You have 0 purchases.";

//how it
$lang['how it works'] = "How it works";

//admin
$lang['alert deletion item'] = "Item deleted";
$lang['alert creation item'] = "Item created";
$lang['alert error'] = "Something was wrong";

//offers
$lang['offered amount'] = "Offered amount";
$lang['desired condition'] = "Desired condition";
$lang['by'] = "By";
$lang['offers for'] = "Offers for your";
$lang['more'] = "More";

//offer
$lang['offer no available'] = "This offer is no longer available";
$lang['contact imperative'] = "Contact ";
$lang['to close the deal'] = " to close the deal";
$lang['item'] = "Item";
$lang['offer by'] = "Offer by";


//how to
$lang['nutshell'] = "In a nutshell";
$lang['selling'] = "Selling";
$lang['buying'] = "Buying";
$lang['fees'] = "Fees";
$lang['nutshell 1'] = "Kloomo is a marketplace for popular pre-owned items, connecting people who own a product with people who want it. The potential buyers make offers and the owner acept one if it seems a good offer. ";
$lang['selling 1'] = "Click on the items you own";
$lang['selling 2'] = "Select the item condition and list it. It's that simple.";
$lang['selling 3'] = "Offers for your items will be visible on the homepage and we also send a weekly newsletter.";
$lang['selling 4'] = "Select the offer that makes sense";
$lang['selling 5'] = "And contact the buyer to arrange the deal. She can pick up the item in your home or in a public place. It's up to you.";
$lang['buying 1'] = "Click on the items you want";
$lang['buying 2'] = "And make a fair offer. It's that simple.";
$lang['buying 3'] = "Sellers will contact you, if your offer makes sense, to arrange the deal and pickup. If not, you will be able to repost your offer, as much as you want during the next year, free of charge.";
$lang['fees 1'] = "Listing fee: 1 credit = 1 year listing period";
//$lang['fees 2'] = "Offer fee: 1 credit = 2 weeks offer period<br>If there was not interested seller for your offer, you can repeat it as much as you want.";
$lang['fees 3'] = "That's all. No hidden fees, no commision.";

//about
$lang['about'] = "About";
$lang['contact'] = "Contact";

//tos
$lang['tos'] = "Terms of service";
$lang['made qc'] ="Made with <i class='icon-heart-empty'></i> in Quebec.";


$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
						
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";


//months
$lang['month_01']="January";
$lang['month_02']="February";
$lang['month_03']="March";
$lang['month_04']="April";
$lang['month_05']="May";
$lang['month_06']="June";
$lang['month_07']="July";
$lang['month_08']="August";
$lang['month_09']="September";
$lang['month_10']="October";
$lang['month_11']="November";
$lang['month_12']="December";



//header
$lang['dashboard'] = "Dashboard";
$lang['questions'] = "Questions";
$lang['logout'] = "Logout";
$lang['signup'] = "Sign up";
$lang['login'] = "Login";

$lang['current_question'] = "Current question";
$lang['plans'] = "Plans";
$lang['settings'] = "Settings";

//home
$lang['home_title'] = "Customer feedback done right";
$lang['home_subtitle'] = "Collect the private & honest information <br> you need to grow your business!<br><br>Great for restaurants, events & hotels.";
$lang['home_cta'] = "FREE trial. No credit card required.";
$lang['how_work'] = "How does it work?";
$lang['step_1'] = "Now your clients can just tap their smartphones";
$lang['step_2'] = "To leave you quick feedback under 5 secs. 100% confidential";
$lang['step_3'] = "That you easily analyze";
$lang['pricing'] = "PRICING";
$lang['basic'] = "Basic";
$lang['pro'] = "Professional";
$lang['business'] = "Corporate";
$lang['free'] = "Free";
$lang['unlimited'] = "Unlimited";
$lang['customer_feedback'] = "Customer feedback";
$lang['receive_kit'] = "Receive Free Satisfacture Intro kit";
$lang['qty_pad'] = "Qty of Pads";
$lang['web_analytics_dashboard'] = "Web analytics dashboard";
$lang['email_support'] = "Email support";
$lang['phone_support'] = "Phone support";
$lang['create_your_look'] = "Create your look";
$lang['creat_your_own_questions'] = "Create your own questions & branding";
$lang['call_us'] = "Call us";

//login
$lang['email'] = "Email";
$lang['password'] = "Password";
$lang['password_reminder'] = "Password reminder";

//pin
$lang['welcome_to_satisfacture'] = "Welcome to Satisfacture";
$lang['your_kit_soon'] ="You will receive your introductory kit soon!";
$lang['enter_pin'] = 'Enter the PIN you received';
$lang['pin_good'] = "Your kit has been activated. Please select your current question from the list below or create a new question";
$lang['pin_bad'] = "Wrong PIN, please try again.";

//settings
$lang['settings'] = "Settings";
$lang['background_color'] = "Background color";
$lang['title_color'] = "Title color";
$lang['option_color'] = "Option color";
$lang['text_color'] = "Text color";
$lang['button_color'] = "Button color";
$lang['edit_settings'] = "Edit settings";
$lang['settings_changed'] = "Your settings have been changed";

//password reminder
$lang['msg_password_sent'] = "An email to retrieve your password will be send soon. Please check your inbox.";
$lang['msg_login_url'] = "Please change your password";

//settings_password
$lang['password_changed'] = "Your password has been changed.";
$lang['change_password'] = "Change password";

$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";


/* 
//FLASH
$lang['msg_password_changed'] 	= "Your password has been changed";
$lang['msg_page_added']		= "<b>This is your page.</b><br>1. <a href='*URL_ADD*'>Edit</a> your page if needed.<br>2. Print & paste your <a href='*URL_QR*'>QR code</a><br>3. That's all!";
$lang['msg_password_sent'] 	= "Check your email inbox (and spam folder). You will find a link to reset your password.";
$lang['msg_login_url']		= "Welcome back! Please change your password.";
$lang['msg_theme_updated'] = "Great! Your theme has been updated.";
$lang['msg_plan_updated'] = "Your plan has been updated.";

//Common
$lang['current_lang'] = "en";
$lang['account'] = "Account";
$lang['login'] = "Login";
$lang['logout'] = "Logout";
$lang['password'] = "Password";
$lang['email'] = "Email";
$lang['submit'] = "Submit";
$lang['tagline'] = "Create your mobile site & QR codes";
$lang['cancel'] = "Cancel";
$lang['delete'] = "Delete";
$lang['signup'] = "Signup";

//account
$lang['create page'] = "Create page";
$lang['change password'] = "Change password";
$lang['change plan'] = "Change plan";
$lang['pages'] = "Pages";
$lang['edit'] = "Edit";
$lang['profile'] = "Profile";
$lang['username'] = "Username";
$lang['homepage'] = "Homepage";
$lang['change homepage'] = "Change homepage";
$lang['the homepage is what people will see at'] ="The homepage is what people will see at";
$lang['do you really want to delete this page?'] ="Do you really want to delete this page?";

//account profile
$lang['change your password'] = "Change your password";
$lang['forgot it'] = "Forgot it?";
$lang['choose wisely'] = "Choose it wisely, because it will be permanent. At least 4 characters.";
$lang['your site will be at'] = "Your site will be:";
$lang["my_username"] = "my_username";
	
//password reminder
$lang['get your password'] = "Get your password";

//page
$lang['qr code'] = "QR Code";
$lang['print qr code'] = "Print QR code";

//footer
$lang['footer phrase'] = "Mobile page powered by";
$lang['TOS'] = "Terms of Service";
$lang['PP'] = "Privacy Policy";
$lang['contact'] = "Contact";

//page code
$lang['your title'] = "Your title";

//plans
$lang['plans'] = "Plans";
$lang['your current plan'] = "Your current plan:";
$lang['free'] = "FREE";
$lang['monthly'] = "monthly";
$lang['became Pro'] = "Become Pro";
$lang['interrupt plan'] = "Cancel Plan";
$lang['signup_plan'] = "Signup";


*/

/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */