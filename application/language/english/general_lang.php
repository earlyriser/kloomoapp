<?php

//months
$lang['month_01']="January";
$lang['month_02']="February";
$lang['month_03']="March";
$lang['month_04']="April";
$lang['month_05']="May";
$lang['month_06']="June";
$lang['month_07']="July";
$lang['month_08']="August";
$lang['month_09']="September";
$lang['month_10']="October";
$lang['month_11']="November";
$lang['month_12']="December";

//footer
$lang['contact_us'] = "Contact Us";
$lang['privacy'] = "Privacy";
$lang['tos'] = "Terms of service";

//header
$lang['dashboard'] = "Dashboard";
$lang['questions'] = "Questions";
$lang['logout'] = "Logout";
$lang['signup'] = "Sign up";
$lang['login'] = "Login";

$lang['current_question'] = "Current question";
$lang['plans'] = "Plans";
$lang['settings'] = "Settings";

//home
$lang['home_title'] = "Customer feedback done right";
$lang['home_subtitle'] = "Collect the private & honest information <br> you need to grow your business!<br><br>Great for restaurants, events & hotels.";
$lang['home_cta'] = "FREE trial. No credit card required.";
$lang['how_work'] = "How does it work?";
$lang['step_1'] = "Now your clients can just tap their smartphones";
$lang['step_2'] = "To leave you quick feedback under 5 secs. 100% confidential";
$lang['step_3'] = "That you easily analyze";
$lang['pricing'] = "PRICING";
$lang['basic'] = "Basic";
$lang['pro'] = "Professional";
$lang['business'] = "Corporate";
$lang['free'] = "Free";
$lang['unlimited'] = "Unlimited";
$lang['customer_feedback'] = "Customer feedback";
$lang['receive_kit'] = "Receive Free Satisfacture Intro kit";
$lang['qty_pad'] = "Qty of Pads";
$lang['web_analytics_dashboard'] = "Web analytics dashboard";
$lang['email_support'] = "Email support";
$lang['phone_support'] = "Phone support";
$lang['create_your_look'] = "Create your look";
$lang['creat_your_own_questions'] = "Create your own questions & branding";
$lang['call_us'] = "Call us";

//login
$lang['email'] = "Email";
$lang['password'] = "Password";
$lang['password_reminder'] = "Password reminder";

//create question
$lang['create_question'] = "Create question";
$lang['type_of_question'] = "Type of question";
$lang['question_textual'] = "Text";
$lang['question_optional'] = "Options";
$lang['question'] = "Question";
$lang['option'] = "Option";
$lang['add_new_option'] = "Add new option";

//question manage
$lang['report'] = "Report";
$lang['view'] = "View";
$lang['make_current_question'] = "Make current question";
$lang['current_question_changed'] = "Your new current question is set";

//question report
$lang['answers_to_your_question'] = "Answers to your question";
$lang['no_answers'] = "There are no answers for this question, yet";

//question show
$lang['thanks_msg'] = "Thanks for your feedback";
$lang['submit'] = "Submit";
$lang['comment'] = "Comment";
$lang['powered'] = "Powered by";

//signup
$lang['username'] = "Username";
$lang['enterprise'] = "Enterprise";
$lang['address'] = "Address";
$lang['state'] = "State/Province";
$lang['phone'] = "Phone";
$lang['contact_person'] = "Name of the contact person";

//pin
$lang['welcome_to_satisfacture'] = "Welcome to Satisfacture";
$lang['your_kit_soon'] ="You will receive your introductory kit soon!";
$lang['enter_pin'] = 'Enter the PIN you received';
$lang['pin_good'] = "Your kit has been activated. Please select your current question from the list below or create a new question";
$lang['pin_bad'] = "Wrong PIN, please try again.";

//settings
$lang['settings'] = "Settings";
$lang['background_color'] = "Background color";
$lang['title_color'] = "Title color";
$lang['option_color'] = "Option color";
$lang['text_color'] = "Text color";
$lang['button_color'] = "Button color";
$lang['edit_settings'] = "Edit settings";
$lang['settings_changed'] = "Your settings have been changed";

//password reminder
$lang['msg_password_sent'] = "An email to retrieve your password will be send soon. Please check your inbox.";
$lang['msg_login_url'] = "Please change your password";

//settings_password
$lang['password_changed'] = "Your password has been changed.";
$lang['change_password'] = "Change password";

$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";
$lang[''] = "";


/* 
//FLASH
$lang['msg_password_changed'] 	= "Your password has been changed";
$lang['msg_page_added']		= "<b>This is your page.</b><br>1. <a href='*URL_ADD*'>Edit</a> your page if needed.<br>2. Print & paste your <a href='*URL_QR*'>QR code</a><br>3. That's all!";
$lang['msg_password_sent'] 	= "Check your email inbox (and spam folder). You will find a link to reset your password.";
$lang['msg_login_url']		= "Welcome back! Please change your password.";
$lang['msg_theme_updated'] = "Great! Your theme has been updated.";
$lang['msg_plan_updated'] = "Your plan has been updated.";

//Common
$lang['current_lang'] = "en";
$lang['account'] = "Account";
$lang['login'] = "Login";
$lang['logout'] = "Logout";
$lang['password'] = "Password";
$lang['email'] = "Email";
$lang['submit'] = "Submit";
$lang['tagline'] = "Create your mobile site & QR codes";
$lang['cancel'] = "Cancel";
$lang['delete'] = "Delete";
$lang['signup'] = "Signup";

//account
$lang['create page'] = "Create page";
$lang['change password'] = "Change password";
$lang['change plan'] = "Change plan";
$lang['pages'] = "Pages";
$lang['edit'] = "Edit";
$lang['profile'] = "Profile";
$lang['username'] = "Username";
$lang['homepage'] = "Homepage";
$lang['change homepage'] = "Change homepage";
$lang['the homepage is what people will see at'] ="The homepage is what people will see at";
$lang['do you really want to delete this page?'] ="Do you really want to delete this page?";

//account profile
$lang['change your password'] = "Change your password";
$lang['forgot it'] = "Forgot it?";
$lang['choose wisely'] = "Choose it wisely, because it will be permanent. At least 4 characters.";
$lang['your site will be at'] = "Your site will be:";
$lang["my_username"] = "my_username";
	
//password reminder
$lang['get your password'] = "Get your password";

//page
$lang['qr code'] = "QR Code";
$lang['print qr code'] = "Print QR code";

//footer
$lang['footer phrase'] = "Mobile page powered by";
$lang['TOS'] = "Terms of Service";
$lang['PP'] = "Privacy Policy";
$lang['contact'] = "Contact";

//page code
$lang['your title'] = "Your title";

//plans
$lang['plans'] = "Plans";
$lang['your current plan'] = "Your current plan:";
$lang['free'] = "FREE";
$lang['monthly'] = "monthly";
$lang['became Pro'] = "Become Pro";
$lang['interrupt plan'] = "Cancel Plan";
$lang['signup_plan'] = "Signup";


*/

/* End of file about_lang.php */
/* Location: ./system/language/english/about_lang.php */