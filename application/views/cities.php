	<div class="boxy central">
		<div class="title"><?php echo lang('change location');?></div>		
		<div class="boxy_content not_padded">			
			<div class="form">
				<ul class="dark_list">
				<?php foreach ( $cities as $city)
				{?>
					<li>
						<a href="<?php echo site_url ('city/'.$city->city_id);?>">
							<?php echo $city->city_name;?>
						</a>
					</li>
				<?php
				}?>					
				</ul>
			</div>			
		</div>	

	</div>