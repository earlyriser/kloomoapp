	<h1>Admin items</h1>
	
	
	<h3>Add item</h3> 
	<form method="post" action="<?php echo site_url('admin/add_item');?>">
		<input type="text" placeholder="item_name" value="" name="item_name"><br>
		<select name="category_id">
			<?php foreach ( $categories as $category)
			{ ?>
				<option value="<?php echo $category->category_id;?>" ><?php echo $category->category_name;?></option>
			<?php 
			} ?>
		</select><br>
		<input class="button" type="submit" value="Submit">
	</form>
	<br>

	<form method="post" action="<?php echo site_url('admin/items_by_country');?>">
		<table>
			<thead>
				<tr>
					<th>Items / Countries</th>
					<?php 
						foreach ( $countries as $country)
						{?>
							<th><?php echo $country->country_name;?></th>
						<?php
						}
					?>				
				</tr>
			</thead>
			<tbody>
			<?php 
				foreach ( $items as $item)
				{?>
				<tr>
					<td><?php echo $item->item_name;?></td>
					
					<?php foreach ( $countries as $country)
					{?>
						<td>
						<?php
						$checked ="";
						if ( $items_countries ){
							foreach ( $items_countries as $item_country){
								if ( $item->item_id == $item_country->item_id AND $country->country_id == $item_country->country_id 
								){
									$checked = "checked";
								}
						}
						}
						?>
						<input type="checkbox" name="<?php echo $country->country_id.'/'.$item->item_id;?>" <?php echo $checked;?>></td>
					<?php
					}
					?>					
				</tr>
				<?php
				}
			?>									
			</tbody>	
		</table>
		<input class="button" type="submit">
	</form>
	<br>

	<h3>Delete item </h3>
	<form method="post" action="<?php echo site_url('admin/delete_item');?>">
		<select name="item_id">
			<?php foreach ( $items as $item)
			{?>
				<option value="<?php echo $item->item_id;?>"><?php echo $item->item_name;?></option>
			<?php
			} ?>
		</select>
		<input class="button" type="submit" value="submit">
	</form>
	<br>	