			<div class="sidepanel">
			<h1><?php echo lang('listings');?></h1>	
			<?php if ( $listings )
			{
				foreach ($listings as $listing) 
				{?>
					<table class="account_listings">
						<tr>
							<td>
								<div class="calendar_square">
									<div class="year"><?php echo substr ( $listing->listing_end, 0, 4 );?></div>
									<div class="day"><?php echo substr ( $listing->listing_end, 8 );?></div>
									<div class="month"><?php echo substr (lang('month_'.substr ( $listing->listing_end, 5, 2 )), 0, 3);?></div>
								</div>
							</td>
							<td>
								<?php echo $listing->item_name;?>,
								<?php echo lang($listing->listing_condition);?>
							</td>
						</tr>
					</table>
				<?php
				}
			}
			else
			{
				echo lang('0 listings');		 
			}?>
			</div>
		</div>
	</div>



