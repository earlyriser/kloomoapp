	<?php //hero y/n
	if( !$this->session->userdata('logged_in'))
	{?>
	<a href="<?php echo site_url('forms/signup');?>"><img class="no_mobile" id="hero" src="<?php echo site_url('assets/img/hero@2x.png');?>"></a>
	<?php
	}
	// instructions if no activity
	if (!$user_listings && !$user_offers) {?>
		<div class="alert warning">
			<?php echo str_replace("*firstname*", $this->session->userdata('user_firstname'), lang('alert no activity') ) ;?>
		</div> <?php
	}
	// you need contact info
	if ( $this->session->userdata('logged_in') && $userdata['user_contact'] == "" &&  is_array ($user_offers) && count( $user_offers) > 0 ) {?>
		<div class="alert danger">
			<?php echo str_replace( "*URL_ADD*", site_url('account/profile_edit'), lang('alert no contact') );?>
		</div>			
	<?php
	}
	?>

	<ul class="menubar">
		<li><a href="#category_1">Tablets</a></li>
		<li><a href="#category_3">PS3</a></li>
		<li><a href="#category_4">XBox 360</a></li>
		<li><a href="#category_5">Wii</a></li>
		<li><a href="#category_6">PS4</a></li>
		<li><a href="#category_7">XBox One</a></li>
		<li><a href="#category_8">Wii U</a></li>

	</ul>

	<div id="show_options" class="hidden_content">
		<span id="show_current" style="text-align:right;">
			<input type="checkbox" id="current_checkbox"  />
			<label for="current_checkbox"><?php echo lang('show current');?></label>
		</span>	

		<?php if ( $user_has_items)
		{?>
		<span id="show_my_items" style="text-align:right;">
			<input type="checkbox" id="my_items_checkbox"  />
			<label for="my_items_checkbox"><?php echo lang('show mine');?></label>
		</span>
		<?php
		}?>
	</div>

	<div id="instructions">
		<div id="instructions_have"></div>
		<div id="instructions_want"></div>
	</div>

	<div class="group" id="your_stuff">
		<ul>
		<?php 
		foreach ($city_object->categories as $category_key => $category ) {?>
			<li>
				<div id="category_<?php echo $category_key;?>" 	class="category"><?php echo lang('category_'.$category_key);?></div>
				<ul class="fake_table">
					<?php foreach ($category->items as $item )
					{?>
						<li class="<?php if ( $item->listings->counter or $item->offers->counter ) echo 'current';?>" data-item_id="<?php echo $item->item_id;?>"
							data-offeri="<?php echo $item->offers->conditions['i']->counter;?>"
							data-offerii="<?php echo $item->offers->conditions['ii']->counter;?>"
							data-offeriii="<?php echo $item->offers->conditions['iii']->counter;?>"
							>
							<span class="have_qty_holder" ><span class="have_qty"><?php if ( $item->listings->counter ) echo $item->listings->counter;?></span></span>
											
				            <a class="have" href="<?php echo site_url('listings/create/'.$item->item_id);?>">have</a>
				            <div class="content">
                                <div class="item_name"><?php echo $item->item_name;?>
                                </div>
				            </div>
				            <a class="want" href="<?php echo site_url('offers/create/'.$item->item_id);?>">want</a>	
				        
							<span class="want_qty_holder" ><span class="want_qty"><?php if ( $item->offers->counter ) echo $item->offers->counter;?></span></span>
				        </li>	
					<?php
					} ?>
													
				</ul>				
			</li>
		<?php			
		}
		?>
		</ul>
	</div>


<script type="text/javascript">
	var main_url = "<?php echo site_url();?>";
	var user_listings = <?php echo json_encode ($user_listings);?>;
	var user_offers = <?php echo json_encode ($user_offers);?>;
	selected_listings ();
	selected_offers ();
	show_cta_offers ();
	show_current ();
</script>
