	<div class="boxy central">
		<div class="title"><?php echo lang('forgot password'); ?>	</div>		
		<div class="boxy_content">			
			<div class="form">	
				<form method="post" action="<?php echo site_url('forms/forgot_password');?>">
					<?php echo lang('reset password instructions');?><br>

					<label for=""><?php echo lang('email'); ?>	</label>
					<input class="input_maxi" type="text" name="email"		value="" >	
			
					<div class="ctas">
						<button type="submit" class="cta primary maxi"><?php echo lang('reset password'); ?>	</button>
					</div>			
				</form>		
			</div>			
		</div>	

	</div>