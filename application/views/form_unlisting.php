	<div class="boxy central">
		<div class="title"><?php echo lang('delete your listing');?></div>		
		<div class="boxy_content">					
				
			<!-- UNLIST -->
			<div id="form_unlist" class="form">
				<form method="post" action="<?php echo site_url('listings/delete/'.$item->item_id);?>">
					<label>Do you want to unlist this item?</label>
					<?php echo $item->item_name;?>
					<input type="hidden" name="item_id"	>
					<div class="ctas">
						<a class="cta secondary modal_close" href="<?php echo site_url();?>">Cancel</a>
						<button type="submit" class="cta primary">Unlist</button>
					</div>			
				</form>
			</div>		

		</div>	

	</div>