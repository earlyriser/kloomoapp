			<div class="sidepanel">	
				<h1>
					<?php echo lang('profile');?> <span class=""><a href="<?php echo site_url('account/profile_edit');?>"><?php echo lang('edit');?></a></span>	
				</h1>
				
				<ul>
					<li><b><?php echo lang('name');?>:</b> <?php echo $user->user_firstname." ".$user->user_lastname;?></li>

					<li><b><?php echo lang('email');?>:</b> <?php echo $user->user_email;?></li>

					<li><b><?php echo lang('city');?>:</b> <?php echo $user->city_id;?></li>

					<li><b><?php echo lang('contact details');?>:</b> <?php echo $user->user_contact;?></li>

					<li><b><?php echo lang('remaining credits');?>:</b> <?php echo $user->user_credits;?></li>
				</ul>
			</div>
		</div>
	</div>



