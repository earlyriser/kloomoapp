	<div class="boxy ">
		<div class="title"><?php echo lang('account');?></div>	
		
		<div class="group not_padded boxy_content ">
			<ul class="sidebar dark_list">
				<li><a <?php if ( $selected=="profile") echo ' class="selected "';?> href="<?php echo site_url('account');?>"><?php echo lang('profile');?></a></li>
<li><a <?php if ( $selected=="offers") echo ' class="selected "';?> href="<?php echo site_url('account/offers');?>"><?php echo lang('offers');?></a></li>				
<li><a <?php if ( $selected=="listings") echo ' class="selected "';?> href="<?php echo site_url('account/listings');?>"><?php echo lang('listings');?></a></li>					
				<li><a <?php if ( $selected=="credits") echo ' class="selected "';?> href="<?php echo site_url('account/credits');?>"><?php echo lang('credits');?></a></li>			
				<li><a <?php if ( $selected=="purchases") echo ' class="selected "';?> href="<?php echo site_url('account/purchases');?>"><?php echo lang('purchases');?></a></li>
				<li><a <?php if ( $selected=="password") echo ' class="selected "';?> href="<?php echo site_url('account/password');?>"><?php echo lang('password');?></a></li>
				<li><a href="<?php echo site_url('forms/logout');?>"><span style="color:#EE5F5B;"><?php echo lang('logout');?></a></span></li>
			</ul>	