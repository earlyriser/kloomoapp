	<?php if ( $listing )
	{?>
	<h1><?php echo lang("offers for");?>: <?php echo $listing->item_name;?></h1>
	<?php 
	} 

	if ( $offers )
	{
	?>
	
	<table id="offers">
		<thead>
			<tr>
				<th><?php echo lang('offered amount');?></th>
				<th><?php echo lang("desired condition");?></th>
				<th><?php echo lang("by");?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ( $offers as $offer)
			{?>
			<tr>
				<td>$<?php echo $offer->offer_amount;?></td>
				<td><?php echo lang( 'i');?></td>
				<td><?php echo $offer->user_firstname;?></td>
				<td><a href="<?php echo site_url('offers/id/'.$offer->offer_id);?>"><span><?php echo lang('more');?></span></a></td>
			</tr>
			<?php
			}
		?>									
		</tbody>	
	</table>
	<?php 
	} ?>
