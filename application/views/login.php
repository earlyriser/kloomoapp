	<div class="boxy central">
		<div class="title"><?php echo lang('login'); ?>	</div>		
		<div class="boxy_content">			
			<div class="form">	
				<form method="post" action="<?php echo site_url('forms/login');?>">
					<label for=""><?php echo lang('email'); ?>	</label>
					<input class="input_maxi" type="email" name="email"		value="">	

					<label for=""><?php echo lang('password'); ?>	</label>
					<input class="input_maxi" type="password" name="password"		value="">
					
					<div style="text-align:right">
						<a href="<?php echo site_url('forms/forgot_password');?>"><?php echo lang('forgot password'); ?></a>
					</div>					
					<div class="ctas">
						<button type="submit" class="cta primary maxi"><?php echo lang('login'); ?>	</button>
					</div>			
				</form>		
			</div>			
		</div>	

	</div>