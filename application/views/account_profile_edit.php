			<div class="sidepanel">	
				<h1>
					<?php echo lang('edit profile');?> <span class=""><a href="<?php echo site_url('account');?>"><?php echo lang('view');?></a></span>	
				</h1>

				<?php if ( validation_errors() ){
					?>
					<div class="alert danger"><?php echo validation_errors(); ?></div>			
					<?php
				}?>

				<form class="form" method="post" action="<?php echo site_url('account/profile_edit');?>">
					<label for=""><?php echo lang('firstname');?> <span class="small"><?php echo lang('public');?></span></label>
					<input class="input_maxi" type="text" name="firstname"		value="<?php echo $user->user_firstname;?>">	

					<label for=""><?php echo lang('lastname');?> <span class="small"><?php echo lang('private');?></span></label>
					<input class="input_maxi" type="text" name="lastname"		value="<?php echo $user->user_lastname;?>">				
	
					<label for=""><?php echo lang('city');?></label>
					<select name="city_id" onchange="request_city_toggle();">
					<?php foreach ( $cities as $city)
					{?>
						<option <?php if ( $city->city_id == $user->city_id) echo 'selected="selected"' ;?> value="<?php echo $city->city_id;?>"><?php echo $city->city_name;?></option>
					<?php
					};?>
						<option <?php if ( $user->city_id == '0') echo 'selected="selected"' ;?> value="0"><?php echo lang('other city');?></option>
					<select>
						
					<label for=""><?php echo lang('contact details');?> <span class="small"><?php echo lang('only visible sellers');?></span></label>
					<textarea class="input_maxi" name="contact"><?php echo $user->user_contact;?></textarea>
					<?php echo lang('contact example');?>

					<div class="ctas">
						<button type="submit" class="cta primary maxi"><?php echo lang('update profile');?></button>
					</div>			
				</form>	
			</div>
		</div>
	</div>



