	<div class="boxy ">
		<div class="title"><?php echo lang('how it works');?></div>	
		
		<div class="group not_padded boxy_content ">
			<ul class="sidebar dark_list">
				<li><a href="#" class="selected tab_switch" data-off="tab_content" data-on="tab_nutshell">In a nutshell</a><li>
				<li><a href="#" class="tab_switch" data-off="tab_content" data-on="tab_selling">Selling</a><li>
				<li><a href="#" class="tab_switch" data-off="tab_content" data-on="tab_buying">Buying</a><li>
					<li><a href="#" class="tab_switch" data-off="tab_content" data-on="tab_fees">Fees</a><li>
			</ul>	

			<div class="sidepanel">	
				<div id="tab_nutshell" class="tab_content">
					<h1><?php echo lang('nutshell');?></h1>
					<p>
						<?php echo lang('nutshell 1');?>
					</p>
					<img class="big_pic" src="<?php echo site_url('assets/img/how_kloomo_works.jpg');?>">
				</div>

				<div id="tab_selling" class="tab_content hidden_content">
					<h1><?php echo lang('selling');?></h1>

					<br>
					<h2><?php echo lang('selling 1');?></h2>
					<img class="big_pic" src="<?php echo site_url('assets/img/ht_have.jpg');?>"><br><br>
					
					<br>
					<h2><?php echo lang('selling 2');?></h2>
					<img class="big_pic" src="<?php echo site_url('assets/img/ht_list.jpg');?>"><br><br>
					
					<br>
					<h2><?php echo lang('selling 3');?></h2>
					<img class="big_pic" src="<?php echo site_url('assets/img/ht_offers_home.jpg');?>"><br><br>
					
					<br>
					<h2><?php echo lang('selling 4');?></h2>
					<img class="big_pic" src="<?php echo site_url('assets/img/ht_offers.jpg');?>"><br><br>
					
					<br>
					<h2><?php echo lang('selling 5');?></h2>
					<img class="big_pic" src="<?php echo site_url('assets/img/ht_contact.jpg');?>"><br><br>
				</div>

				<div id="tab_buying" class="tab_content hidden_content">
					<h1><?php echo lang('buying');?></h1>
					
					<br>
					<h2><?php echo lang('buying 1');?></h2>
					<img class="big_pic" src="<?php echo site_url('assets/img/ht_want.jpg');?>"><br><br>
					
					<br>
					<h2><?php echo lang('buying 2');?></h2>
					<img class="big_pic" src="<?php echo site_url('assets/img/ht_offer.jpg');?>"><br><br>				
				
					<br>
					<h2><?php echo lang('buying 3');?></h2>
					<br>
				</div>

				<div id="tab_fees" class="tab_content hidden_content">
					<h1><?php echo lang('fees');?></h1>

					<p>
						<?php echo lang('fees 1');?>
					</p>

					<p>
						<?php echo lang('fees 3');?><br>
						<img src="<?php echo site_url('assets/img/cc_visa.png');?>" alt="Visa">
						<img src="<?php echo site_url('assets/img/cc_mastercard.png');?>" alt="Mastercard">
						<img src="<?php echo site_url('assets/img/cc_amex.png');?>" alt="American Express">
					</p>			
				</div>				
			</div>
		</div>
	</div>

	<script>
	  	$('.tab_switch').click(function(){
	  		//vars
	  		var on_id = $(this).attr("data-on");
	  		var off_class = $(this).attr("data-off");

	  		//tabs
			$('.tab_switch').removeClass('selected');
	  		$(this).addClass('selected');

	  		//content
	  		$('.'+off_class).hide();
	  		$('#'+on_id).show()
	  	});
	  	mixpanel.track("Howto visit");
	  </script>
