
	<div id="footer">
		<a href="<?php echo site_url('pages/about');?>"><?php echo lang('about');?></a>&nbsp;&nbsp;&nbsp;
		<a href="<?php echo site_url('pages/tos');?>"><?php echo lang('tos');?></a>&nbsp;&nbsp;&nbsp;
		<a href="<?php echo site_url('pages/contact');?>"><?php echo lang('contact');?></a>&nbsp;&nbsp;&nbsp;
		<span class="no_mobile"><?php echo lang('made qc');?> </span>
	</div>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-43677635-1', 'kloomo.com');
		ga('send', 'pageview');
	</script>	
</body>
</html>