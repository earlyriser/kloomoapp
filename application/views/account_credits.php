			<div class="sidepanel">

				<h1><?php echo lang('get free credits');?></h1>	
				<?php echo lang('referral 1')." ".$this->config->item('credits_referral')." ".lang('referral 2');?>.<br><br>
				<?php echo lang('referral 3');?><br>				
				<div class="form">
					<input id="referral_field" class="input_maxi" type="text" value="<?php echo site_url('referrals/id/'.$this->session->userdata('user_id'));?>">
				</div>
<br><br>
								
				<h1>Buy Credits				<img style="vertical-align:middle" src="<?php echo site_url('assets/img/cc_visa.png');?>" alt="Visa">
				<img style="vertical-align:middle" src="<?php echo site_url('assets/img/cc_mastercard.png');?>" alt="Mastercard">
				<img style="vertical-align:middle" src="<?php echo site_url('assets/img/cc_amex.png');?>" alt="American Express"></h1>

				<div id="processing_payment" class="alert warning hidden_content">
					<?php echo lang('processing payment');?>
				</div>
				
				<form action="" method="POST">
					<input type="hidden" id="form_package" name="package" value="15">
					<script src="https://checkout.stripe.com/v2/checkout.js"></script>

				<?php foreach ($packages as $package) 
				{?>
				  <div  value="<?php echo $package->price;?>" data-credits="<?php echo $package->credits+$package->free;?>" class="paymentButton <?php if ( $package->credits == 30 ) echo "primary";?>">
				  	<span><?php echo $package->credits;?></span><br>
				  	<?php echo lang('credits');?><br><br>
				  	<?php if ( $package->free != "0"){
				  		?>
				  		+<?php echo $package->free;?> free<br><br>
				  	<?php	
				  	} ?>
				  	
				  	<div class="price success">
				  		$<?php echo $package->price;?>
				  	</div>				  	
				  </div>
				<?php 
				} ?>
 
				  <script>
				  	$('.paymentButton').click(function(){
				  		var amount = $(this).attr("value");
				  		var cents = amount * 100;
				  		var credits = $(this).attr("data-credits");

					    var token = function(res){
					        var $input = $('<input type="hidden" name="stripeToken" />').val(res.id);
					        $('#form_package').val(amount);
					        $('form').hide();
					        $('#processing_payment').show();
					        $('form').append($input).submit();
					    };
				    	var params = {
					        key:         '<?php echo $this->config->item("stripe_live_public_key");?>',
					        amount:      cents,
					        currency:    'usd',
					        name:        'Kloomo',
					        description: credits+' credits',
					        panelLabel:  'Checkout',
					        token:       token
				      	};

					      StripeCheckout.open(params);

				    	return false;
				    });
				  </script>
				   
				</form>
			</div>
		</div>
	</div>

<script>
$("#referral_field").on("click", function () {
   $(this).select();
});
</script>

