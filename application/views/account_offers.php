			<div class="sidepanel">
			<h1><?php echo lang('offers');?></h1>	
			<?php if ( $offers )
			{
				foreach ($offers as $offer) 
				{?>
					<table class="account_offers">
						<tr>
							<td>
								<div class="calendar_square">
									<div class="year"><?php echo substr ( $offer->offer_end, 0, 4 );?></div>
									<div class="day"><?php echo substr ( $offer->offer_end, 8 );?></div>
									<div class="month"><?php echo substr (lang('month_'.substr ( $offer->offer_end, 5, 2 )), 0, 3);?></div>
								</div>
							</td>
							<td>
								$<?php echo $offer->offer_amount;?> for
								<?php echo $offer->item_name;?>,
								<?php echo lang($offer->offer_condition);?>
							</td>
						</tr>
					</table>
				<?php
				}
			}
			else
			{
				echo lang('0 offers');		 
			}?>
			</div>
		</div>
	</div>



