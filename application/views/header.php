<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset='utf-8'> 
	<meta name="description" content="<?php echo lang('meta description');?>">	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/stylesheets/screen.css');?>" media="screen" >
	<link rel="stylesheet" href="<?php echo site_url('assets/css/font-awesome/css/font-awesome.min.css');?>">
	<link rel="shortcut icon" href="<?php echo site_url('assets/img/favicon.ico');?>">
	<link rel="apple-touch-icon-precomposed" href="<?php echo site_url('assets/img/apple-touch-icon-precomposed.png');?>">
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo site_url('assets/img/apple-touch-icon-76x76-precomposed.png');?>">
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo site_url('assets/img/apple-touch-icon-152x152-precomposed.png');?>">
	<script type="text/javascript">var glb_url="<?php echo site_url();?>";</script>
	<script src="<?php echo site_url('assets/js/underscore-min.js');?>"></script>
	<script src="<?php echo site_url('assets/js/jquery-1.8.2.min.js');?>"></script>
	<script src="<?php echo site_url('assets/js/main.js');?>"></script>

<!-- start Mixpanel --><script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);
b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
mixpanel.init("6f9a484e5510564caba52eb54debd0db");</script><!-- end Mixpanel -->
</head>
<body>
	<div id="header" class="group">
		<a href="<?php echo site_url(); ?>"><img src="<?php echo site_url('assets/img/logo_kloomo@2x.png') ;?>" style="align:bottom;width:122px;"></a>

		<?php if ( isset( $city_object ) ) 
		{
			if ( isset($user_logged) AND $user_logged ) 
			{?>
					<span id="location"><?php echo $city_object->city_name;?></span>
			<?php
			}else
			{?>
					<a id="location" href="<?php echo site_url('cities');?>">
					<?php echo $city_object->city_name;?>
					<i class="icon-angle-down"></i> </a>				
			<?php
			}		
		}
		?>

		<div id="nav">
			<ul class="group">
			<?php 
				if($this->session->userdata('logged_in')) {?>
				<li><a href="<?php echo site_url('pages/howto');?>"><span class="view_text"><?php echo lang('how it works');?></span><span class="view_icon"><i class="icon-question" title=""></i></span></a></li>
				<li>
					<?php 
					$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $this->session->userdata('user_email')) ) ) ."?s=30&d=mm";	
					?>
					
					<a href="<?php echo site_url('account');?>"><img alt="avatar" src="<?php echo $grav_url;?>" alt=""></a>
					<a class="no_mobile" href="<?php echo site_url('account/credits');?>"><i class="icon-money" title="<?php echo lang('credits');?>"></i> <?php echo $this->session->userdata('user_credits');?></a>
				</li>
			<?php 
			}else{?>
				<li><a href="<?php echo site_url('forms/login');?>"><?php echo lang('login');?></a></li>
				<li><a href="<?php echo site_url('forms/signup');?>"><?php echo lang('signup');?></a></li>
				<li><a href="<?php echo site_url('pages/howto');?>"><span class="view_text"><?php echo lang('how it works');?></span><span class="view_icon"><i class="icon-question" title=""></i></span></a></li>
			<?php 
			}?>								
			</ul>
		</div>		
	</div>


<?php if (0)
{?>
	<div id="breadcrumbs" class="group">
		<ul>
			<li><a href="#">Home</a></li>
			<li><a href="#">Offers: Ipad 3rd generation</a></li>
			<li><a href="#">Offer from Jack</a></li>
		</ul>
	</div>
<?php 
}?>


	<?php if ( $this->session->flashdata('alert') ){
		$alert = $this->session->flashdata('alert');?>
		<div class="alert <?php echo $alert->type;?>"><?php echo lang($alert->content);?></div>
		<?php
	}?>
