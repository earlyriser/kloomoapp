			<div class="sidepanel">	
				<h1>
					<?php echo lang('reset password');?>
				</h1>

				<?php if ( validation_errors() ){
					?>
					<div class="alert danger"><?php echo validation_errors(); ?></div>			
					<?php
				}?>
		
				<form class="form" method="post" action="<?php echo site_url('account/password');?>">
					<label for=""><?php echo lang('password');?></label>
					<input class="input_maxi" type="text" name="password"		value="">				
	
					<div class="ctas">
						<button type="submit" class="cta primary maxi"><?php echo lang('reset password');?></button>
					</div>			
				</form>	
			</div>
		</div>
	</div>



