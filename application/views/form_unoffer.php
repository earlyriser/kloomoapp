	<div class="boxy central">
		<div class="title">Retire your offer</div>		
		<div class="boxy_content">					
			<!-- UNOFFER -->
			<div id="form_unoffer" class="form">
				<form method="post" action="<?php echo site_url('offers/delete/'.$offer->item_id);?>">
					<label>Do you want to retire this offer?</label>

					<b><?php echo lang('item');?>: </b><?php echo $offer->item_name;?><br>
					<b><?php echo lang('desired condition');?>: </b><?php echo lang($offer->offer_condition);?><br>
					<b><?php echo lang('offered amount');?>: </b>$<?php echo $offer->offer_amount;?>
					<input type="hidden" name="item_id"	>
					<div class="ctas">
						<a class="cta secondary modal_close" href="<?php echo site_url();?>">Cancel</a>
						<button type="submit" class="cta primary unoffering_button">Retire offer</button>
					</div>			
				</form>
			</div>	
								
		</div>	

	</div>