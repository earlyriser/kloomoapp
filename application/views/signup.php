	<div class="boxy central">
		<div class="title"><?php echo lang('signup');?></div>		
		<div class="boxy_content">			
			<div class="form">	
				<form method="post" action="<?php echo site_url('forms/signup/'.$referral_id);?>">

		<?php if ( validation_errors() ){
			?>
			<div class="alert danger"><?php echo validation_errors(); ?></div>			
			<?php
		}?>

					<label for=""><?php echo lang('firstname');?></label>
					<input class="input_maxi" type="text" name="firstname" value="<?php echo set_value('firstname'); ?>">
					
					<label for=""><?php echo lang('lastname');?></label>
					<input class="input_maxi" type="text" name="lastname" value="<?php echo set_value('lastname'); ?>"	>

					<label for=""><?php echo lang('email');?></label>
					<input class="input_maxi" type="email" name="email"	value="<?php echo set_value('email'); ?>">	

					<label for=""><?php echo lang('password');?></label>
					<input class="input_maxi" type="password" name="password" value="">

					<label for=""><?php echo lang('city');?></label>
					<select name="city_id" onchange="request_city_toggle();">
					<?php foreach ( $cities as $city)
					{?>
						<option value="<?php echo $city->city_id;?>"><?php echo $city->city_name;?></option>
					<?php
					};?>
						<option value="0"><?php echo lang('other city');?></option>
					<select>

					<div id="requested_city" class="hidden_content">
						<br>
						<label for=""><?php echo lang('your city');?></label>
						<input class="input_maxi" type="text" name="request_city"	value="<?php echo set_value('request_city'); ?>"	placeholder="empty">	
					</div>


					<div>
						<br>
						<input onchange="newsletter_toggle();" type="checkbox" checked="checked" name="newsletter"> 
						<?php echo lang('receive newsletter');?><br> 
						<div id="newsletter_question" class="hidden_content alert warning"><?php echo lang ('newsletter warning');?></div>
					</div>
					<div class="ctas">
						<button type="submit" class="cta primary maxi"><?php echo lang('create account');?></button>
					</div>			
				</form>		
			</div>			
		</div>	

	</div>

<script type="text/javascript">
function request_city_toggle()
{
    var selected_city = $('select[name="city_id"]').val();
    if ( selected_city == '0'){
    	$("#requested_city").show('fast');
    }
    else{
    	$("#requested_city").hide('fast');
    }
}

function newsletter_toggle()
{
    if ( $('input[name="newsletter"]').is(':checked')){
    	$("#newsletter_question").hide('fast');
    }
    else{
    	$("#newsletter_question").show('fast');
    }
}

mixpanel.track("Signup visit");
</script>	