	<div class="boxy central">
		<div class="title"><?php echo lang('create your listing');?></div>		
		<div class="boxy_content">					
				<div id="form_listing" class="form">		
					<form method="post" action="<?php echo site_url('listings/create/'.$item->item_id);?>">
						
						<b>Item:</b> <?php echo $item->item_name;?>

						<label for="listing_condition">What's the condition of this item?</label>
				
						<div class="conditions group">
							<div class="condition condition1" data-condition="iii"><?php echo lang('iii');?></div>
							<div class="condition condition2" data-condition="ii"><?php echo lang('ii');?></div>
							<div class="condition condition3" data-condition="i"><?php echo lang('i');?></div>
						</div>

						<?php if ($def_prefix){
						?>
						<div class="conditions_definitions">
							<div class="condition_definition hidden_content" data-condition="iii"><ul><?php echo lang($def_prefix.' iii');?></ul></div>
							<div class="condition_definition hidden_content" data-condition="ii"><ul><?php echo lang($def_prefix.' ii');?></ul></div>
							<div class="condition_definition hidden_content" data-condition="i"><ul><?php echo lang($def_prefix.' i');?></ul></div>
						</div>					
						<?php 	
						}?>

						<input type="hidden" name="condition" >
						
						<div class="ctas">
							<a class="cta secondary modal_close" href="<?php echo site_url();?>">Cancel</a>
							<button type="submit" class="cta primary listing_button">List for sale</button>
						</div>			
					</form>		
				</div>	
						
		</div>	

	</div>