<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ------------------------------------------------------------------------

//----------------------------------------------
//           GENERAL FUNCTIONS
//----------------------------------------------


//-----------------------------------------------
// GET CITY OBJECT
//-----------------------------------------------
if ( ! function_exists('get_city_object'))
{
	function get_city_object ( $city_id )
	{

		//vars
		$today = date("Y-m-d");
		$main_object = (object)array('city' => $city_id, 'city_name'=> '', 'categories' => array());

		//model
 		$CI = get_instance();	
		$CI->load->model('Model_listing');
		$CI->load->model('Model_offer');
		$CI->load->model('Model_item');

//$CI->output->enable_profiler(TRUE);

		//queries
		$items = $CI->Model_item->get ( set_items_query ( $city_id ));	
		$items_ids_available = extract_item_ids ( $items );

		//process items
		$offer_type = array ( "counter" => 0, "total" => 0, "max" => null, "min" => null, "avg" => 0);
		$current_category = "";
		if ( $items )
		{
			foreach ($items as $item ) {
				//$main_object->categories[$item->category_id];

				$item->listings = (object)array('counter' => 0, 'conditions' => array ("i" => 0, "ii" => 0, "iii" => 0));
				$item->offers = (object)array('counter' => 0, 'conditions' => array ("i" => (object)$offer_type, "ii" => (object)$offer_type, "iii" => (object)$offer_type ));
				$main_object->categories[$item->category_id]->items[$item->item_id] = $item;
			}
			
			$main_object->city_name = $items[0]->city_name;
		}
		$items = null; //clean

		//process listings
		$listings = $CI->Model_listing->get( set_listings_query ( $city_id, $today ) );	
		if ( $listings ){
			foreach ($listings as $listing) {
				if ( in_array( $listing->item_id, $items_ids_available) ){
					$main_object->categories[$listing->category_id]->items[$listing->item_id]->listings->conditions[$listing->listing_condition] = $main_object->categories[$listing->category_id]->items[$listing->item_id]->listings->conditions[$listing->listing_condition]+1;
					$main_object->categories[$listing->category_id]->items[$listing->item_id]->listings->counter++; 
				}
			}
		}
		$listings = null;

		//process offers
		$offers = $CI->Model_offer->get( set_offers_query ( $city_id, $today ) );	
		$current_offer_item = $previous_offer_item = "";
		if ( $offers ){
			foreach ( $offers as $offer ){
				if ( in_array ( $offer->item_id, $items_ids_available)){
					$main_object->categories[$offer->category_id]->items[$offer->item_id]->offers->counter++;

					$temp = $main_object->categories[$offer->category_id]->items[$offer->item_id]->offers->conditions[$offer->offer_condition];
					//$main_object->items[$listing->item_id]->offers[$offer->offer_condition]->blabla = $offer->offer_amount;
					$temp->counter++;
					$temp->total += $offer->offer_amount;

					//min
					if ( $temp->min == null OR $offer->offer_amount < $temp->min ){
						$temp->min = $offer->offer_amount;
					}

					//max
					if ( $temp->max == null OR $offer->offer_amount > $temp->max ){
						$temp->max = $offer->offer_amount;
					}

					//avg
					$temp->avg = $temp->total / $temp->counter;
				}
			}
		}
		$offers = null;
		return ( ($main_object) ) ;

	}
}

//-----------------------------------------------
// EXTRACT ITEM IDS
//-----------------------------------------------
if ( ! function_exists('extract_item_ids'))
{
	function extract_item_ids ( $items ){
		$result = array();
		if ( $items )
		{
			foreach ($items as $item) {
				if ( !in_array( $item->item_id, $result)){
					array_push( $result, $item->item_id);
				}
			}
		}
		return $result;
	}
}


//-----------------------------------------------
// SET ITEMS QUERY
//-----------------------------------------------
if ( ! function_exists('set_items_query'))
{
	function set_items_query( $current_city ){
		$query_params = new StdClass;
		$query_params->select = "items.item_id, category_id, item_name, items_countries.country_id, country_name, country_lang, city_id, city_name";
		$query_params->joins = array( 
			"items_countries" => 'items_countries.item_id = items.item_id',
			"countries" => 'countries.country_id = items_countries.country_id',
			"cities" => 'cities.country_id = countries.country_id'
		);
		$query_params->order = array( 
				"category_id" => 'ASC',
				"item_name" => 'ASC'
			);		
		$query_params->where = array( 
			"city_id" => $current_city
		);
		return $query_params;
	}
}

//-----------------------------------------------
// SET OFFERS QUERY
//-----------------------------------------------
if ( ! function_exists('set_offers_query'))
{
	function set_offers_query( $current_city, $today ){
		$query_params = new StdClass;
		$query_params->where = array( 
				"city_id" => $current_city,
				"offer_end >="	=> $today
			);
		$query_params->joins = array( 
				"users" => 'users.user_id = offers.user_id',
				"items" => 'items.item_id = offers.item_id'
			);
		$query_params->order = array( 
				"offers.item_id" => 'ASC'
			);
		return $query_params;
	}
}

//-----------------------------------------------
// SET LISTINGS QUERY
//-----------------------------------------------
if ( ! function_exists('set_listings_query'))
{
	function set_listings_query( $current_city, $today ){
		$query_params = new StdClass;
		//$query_params->select = "listing_id";		
		$query_params->where = array( 
				"city_id" => $current_city,
				"listing_end >="	=> $today
			);
		$query_params->joins = array( 
				"users" => 'users.user_id = listings.user_id',
				"items" => 'items.item_id = listings.item_id'
			);
		return $query_params;			
	}
}

//-----------------------------------------------
// GET DEF PREFIX
//-----------------------------------------------
if ( ! function_exists('common_get_def_prefix'))
{
	function common_get_def_prefix ( $category_id){	
		$category_id = (int)$category_id;
		switch ($category_id) {
			case (1):
		       return "tabletphones";
		       break;
			case (2):
		       return "tabletphones";
		       break;		       
			default:
				return "games";
		}
		return false;
	}
}

//-----------------------------------------------
// GET USER LISTINGS
// R: Array of current listed items by user
/* Ex:   
	array(2) {
		["ipad_2_wifi_3g_32gb"]=> string(2) "++"
  		["ipad_4th_wifi_cellular_128gb"]=> string(1) "+"
	}
*/
//-----------------------------------------------
if ( ! function_exists('get_user_listings'))
{
	function get_user_listings( $user_id ){
		$result = array();

		//model
 		$CI = get_instance();	
		$CI->load->model('Model_listing');

		//param & query
		$query_params = new StdClass;
		$query_params->select = "item_id, listing_condition, listing_end";		
		$query_params->where = array( 
			"listing_end >=" => date("Y-m-d"),
			"user_id" => $user_id
		);
		$listings = $CI->Model_listing->get( $query_params );

		//process
		if ( $listings ){
			foreach ( $listings as $listing){
				$result[ $listing->item_id] = $listing->listing_condition;
			}
		}

		return ( $result );			
	}
}


//-----------------------------------------------
// GET USER OFFERS
// R: Array of current offers by user
/* Ex:   
*/
//-----------------------------------------------
if ( ! function_exists('get_user_offers'))
{
	function get_user_offers( $user_id ){
		$result = array();

		//model
 		$CI = get_instance();	
		$CI->load->model('Model_offer');

		//param & query
		$query_params = new StdClass;
		$query_params->select = "item_id, offer_condition, offer_end";		
		$query_params->where = array( 
			"offer_end >=" => date("Y-m-d"),
			"user_id" => $user_id
		);
		$offers = $CI->Model_offer->get( $query_params );

		//process
		if ( $offers ){
			foreach ( $offers as $offer){
				$result[ $offer->item_id] = $offer->offer_condition;
			}
		}

		return ( $result );			
	}
}


if ( ! function_exists('create_alert'))
{
	function create_alert( $type, $content ){
		$CI = get_instance();	
		$alert = new StdClass;
		$alert->type = $type;
		$alert->content = $content; 
		$CI->session->set_flashdata('alert', $alert);
	}
}

if ( ! function_exists('common_is_valid_condition'))
{
	function common_is_valid_condition( $c=false ){
		if ( $c == "iii" || $c=='ii' || $c=='i')
			return true;
		return false;
	}
}


if ( ! function_exists('has_credits'))
{
 	function has_credits( $user ){
		if ( $user->user_credits < 1 ){
			//alert
			create_alert ( "danger", "alert no credits");
			return false;		
		}
		else{
			return true;
		}
	}
}

if ( ! function_exists('update_credits'))
{
	function update_credits ( $credits, $user, $user_is_logged = true ){
		//load
 		$CI = get_instance();	
		$CI->load->model('Model_user');

		//params
		$query_params = new StdClass;
		$query_params->where = array(
				"user_id" => $user->user_id
		);
		//calc new credits
		$credits = $user->user_credits + $credits;
		//db
		$data = array('user_credits' => $credits);
		$CI->Model_user->update( $query_params, $data);

		//userdata
		if ( $user_is_logged ){
			$CI->session->set_userdata('user_credits', $credits);
		}
	}
}
