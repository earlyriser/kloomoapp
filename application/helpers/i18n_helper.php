<?php

//-----------------------------------------------
function get_lang ( )
//-----------------------------------------------
{

	$CI =& get_instance();
	$default_lang = $CI->config->item('language');
	$session_lang = $CI->session->userdata('lang');  
	
	if ( $session_lang == false )
	{	
		$session_lang = $default_lang;
		$CI->session->set_userdata('lang', $session_lang );
	}
	else
	{
		$CI->config->set_item('language', $session_lang);
	}
	return $session_lang;
}
