$(document).ready(function()
{       
                
    //listing form, condition click
    $('.condition').click (function () {
        var condition = $(this).attr('data-condition');
        $(this).siblings().removeClass('selected');
        $(this).addClass('selected');
        $(this).closest('.form').find('input[name="condition"]').val( condition );

        $('.condition_definition').hide();
        $('.condition_definition[data-condition="'+condition+'"]').show();
    });

    //listing form, validation
    $('.listing_button').click( function ( event ){

        event.preventDefault();
        var value_condition = $('#form_listing input[name="condition"]').val();
        var submit = true;
        var field_condition = $('label[for="listing_condition"] ');

        //check condition
        if ( value_condition == 'i' || value_condition == "ii" || value_condition== "iii"){
            field_condition.removeClass("form_error");
        }
        else {  //error
            field_condition.addClass("form_error");
            submit = false;                   
        }

        //if ok, submit
        if ( submit ){
            $('#form_listing form').submit();
        }
    });


    //offer form, validation
    $('.offer_button').click( function ( event ){

        event.preventDefault();
        var value_condition = $('#form_offer input[name="condition"]').val();
        var value_amount = parseInt ( $('#form_offer input[name="amount"]').val() );
        var submit = true;
        var field_condition = $('label[for="offer_condition"] ');
        var field_amount = $('label[for="offer_amount"] ');

        //check condition
        if ( value_condition == 'i' || value_condition == "ii" || value_condition== "iii"){
            field_condition.removeClass("form_error");
        }
        else {  //error
            field_condition.addClass("form_error");
            submit = false;                   
        }

        //check amount
        if ( isNaN( value_amount ) ){
            field_amount.addClass("form_error"); 
            submit = false;
        }
        else{
            field_amount.removeClass("form_error");
        }

        //if ok, submit
        if ( submit ){
            $('#form_offer form').submit();
        }
    });

    //toggle my items
    if ( $('#show_my_items').length >0)
    {
        $('#show_my_items input[type="checkbox"]').change(function() {
            if($(this).is(":checked")) {
                $('#show_current input[type="checkbox"]').prop('checked', false);
                $(".fake_table li").hide();
                $("#instructions").hide();                
                $(".have.selected").parent().show();
                $(".want.selected").parent().show();
            }else{
                $(".fake_table li").show();
            }               
        });
    }
});

//selected listings
function selected_listings (){
    _.each ( user_listings, function ( value, key ){     
        $("div [data-item_id="+key+"] .have").addClass('selected '+convert_to_class(value) ).attr("href", main_url+"listings/delete/"+key);
    })
}

//selected offers
function selected_offers (){
    _.each ( user_offers, function ( value, key ){
        $("div [data-item_id="+key+"] .want").addClass('selected '+convert_to_class(value) ).attr("href", main_url+"offers/delete/"+key);
    })
}

//show current listings/offers
function show_current(){
    //show checkbox
    if ( $('.current').length > 0){
        $('#show_options').show();   
    } 
    //show current
    if ( $('#show_current').length >0)
    {
        $('#show_current input[type="checkbox"]').change(function() {
            if($(this).is(":checked")) {
                $('#show_my_items input[type="checkbox"]').prop('checked', false);
                $(".fake_table li").hide();
                $("#instructions").hide();                
                $(".current").show();
            }else{
                $(".fake_table li").show();
            }               
        });
    } 
}

//show user cta offers
function show_cta_offers(){
    _.each ( user_listings, function (value, key){   
        var condition = value;
        var results = 0;
        var offersi = parseInt( $("li[data-item_id="+key+"]").attr('data-offeri') );
        var offersii = parseInt( $("li[data-item_id="+key+"]").attr('data-offerii') );
        var offersiii = parseInt( $("li[data-item_id="+key+"]").attr('data-offeriii') );
    
        switch(condition)
        {    
            case 'i':
              results =  offersi;
              break;
            case 'ii':
              results = offersi + offersii;
              break; 
            case 'iii':
              results = offersi + offersii + offersiii;
              break;
        } 

        if( results){
            $("li[data-item_id="+key+"] .item_name").prepend('<span class="offers"><a href="'+glb_url+'offers/item/'+key+'">'+results+' offers</a></span>');
        }

    })
}



function convert_to_class( str){
switch(str)
    {    
        case 'i':
          return 'cond1'
          break;
        case 'ii':
          return 'cond2'
          break; 
        case 'iii':
          return 'cond3'
          break;
    }      
}





