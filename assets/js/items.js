var user_listings =
{      "items":
      [
        { 
          "name":"Ipad 3" ,
          "condition": "good"
        },
        { 
          "name":"Wii" ,
          "condition": "good"
        }
]
}

var user_offers =
{      "items":
      [
        { 
          "name":"Ipad" ,
          "condition": "new"
        },
        { 
          "name":"PS3" ,
          "condition": "good"
        }
]
}

var list = {
  "categories": 
  [
    {
      "name":"tablets",
      "items":
      [
        { 
          "name":"Ipad" ,
          "offer_ok":true,
          "list_ok":true,
          "offers":96,
          "new":35,
          "good":49,
          "usable":10
        },
        { 
          "name":"Ipad 3" ,
          "offer_ok":true,
          "list_ok":true 
        }
]
    },
    {
      "name":"consoles",
      "items":
      [
        { 
          "name":"PS3" ,
          "offer_ok":true,
          "list_ok":true,
          "have": true,
          "want": true 
        },
        { 
          "name":"Wii" ,
          "offer_ok":true,
          "list_ok":true,
          "have": false,
          "want": true 
        }
]
    }
  ]
};